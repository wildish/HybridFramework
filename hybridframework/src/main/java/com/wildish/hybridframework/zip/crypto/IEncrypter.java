/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.zip.crypto;

import com.wildish.hybridframework.zip.exception.ZipException;

public interface IEncrypter {
	
	public int encryptData(byte[] buff) throws ZipException;
	
	public int encryptData(byte[] buff, int start, int len) throws ZipException;
	
}
