/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.zip.crypto;

import com.wildish.hybridframework.zip.exception.ZipException;

public interface IDecrypter {
	
	public int decryptData(byte[] buff, int start, int len) throws ZipException;
	
	public int decryptData(byte[] buff) throws ZipException;
	
}
