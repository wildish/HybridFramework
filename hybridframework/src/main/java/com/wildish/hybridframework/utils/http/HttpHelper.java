/*
 * Created by Wildish on 2016-11-16
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils.http;
import com.wildish.hybridframework.utils.HttpBaseHelper;
import com.wildish.hybridframework.utils.sign.EncryptUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;

public class HttpHelper extends HttpBaseHelper {

    public static String get(String url, HashMap<String, String> params, int key) throws NoSuchAlgorithmException,
            NoSuchProviderException, IOException, KeyManagementException {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", CONTENT_TYPE_TEXT);
        headers.put("auth_key", String.valueOf(key));
        return doGet(url, EncryptUtil.encode(params, key), headers);
    }

    public static String get(String url, JSONObject params)
            throws KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", CONTENT_TYPE_JSON);
        return doGet(url, params.toString(), headers);
    }

    public static String get(String url, JSONObject params, HashMap<String, String> headers)
            throws KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        return doGet(url, params.toString(), headers);
    }
}
