/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils;

/**
 * Created by Keith on 2015-03-28.
 */
public class CustomClickUtil {

    private static CustomClickUtil instance;
    public static CustomClickUtil getInstance() {
        if(instance == null) {
            instance = new CustomClickUtil();
        }
        return instance;
    }

    private long lastClickTime;
    public synchronized boolean isFastClick() {
        long time = System.currentTimeMillis();
        if ( time - lastClickTime < 1000) {
            return true;
        }
        lastClickTime = time;
        return false;
    }
}
