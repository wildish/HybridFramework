/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils.audio;

public interface ISoundHelper {
    boolean loadSoundById(String key, SoundItem item);

    boolean loadSoundByPath(String key, SoundItem item);

    void playSound(String key, String status);

    void releaseAll();
}
