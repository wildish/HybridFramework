/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils;

import android.view.View;

public abstract class OnCustomClickListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        CustomClickUtil util = CustomClickUtil.getInstance();
        if(!util.isFastClick()) {
            onCustomClick(view);
        }
    }
    public abstract void onCustomClick(View v);
}
