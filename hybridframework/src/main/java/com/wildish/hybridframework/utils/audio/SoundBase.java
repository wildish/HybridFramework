/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils.audio;

import android.content.Context;


import java.util.HashMap;

public abstract class SoundBase implements ISoundHelper {

    protected Context context;
    protected HashMap<String, SoundItem> sMap = new HashMap<>();
    protected SoundItem currentSound;
    public static final int DEFAULT_SOUNDPOOL_SIZE = 10;

    public SoundBase(Context context) {
        this.context = context;
    }
}
