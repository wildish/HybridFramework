/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils.audio;

import android.media.MediaPlayer;

/**
 * Created by Keith on 2015-04-02.
 */
public class SoundItem {
    private int rawId;
    private int priority = 0;
    private int loop = 0;
    private float rate = 1.0f;
    private String soundPath;
    private MediaPlayer player;
    private String key;

    public static final int STATUS_UNLOAD = -1;
    public static final int STATUS_LOADED = 0;
    public static final int STATUS_PLAYING = 1;
    public static final int STATUS_PAUSE = 2;

    private int status = STATUS_UNLOAD;
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public MediaPlayer getPlayer() {
        return player;
    }

    public void setPlayer(MediaPlayer player) {
        this.player = player;
    }

    public String getSoundPath() {
        return soundPath;
    }

    public void setSoundPath(String soundPath) {
        this.soundPath = soundPath;
    }

    private int streamId;

    public SoundItem(){}

    public SoundItem(int rawId) {
        this.rawId = rawId;
    }

    public int getRawId() {
        return rawId;
    }

    public void setRawId(int rawId) {
        this.rawId = rawId;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getLoop() {
        return loop;
    }

    public void setLoop(int loop) {
        this.loop = loop;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getStreamId() {
        return streamId;
    }

    public void setStreamId(int streamId) {
        this.streamId = streamId;
    }
}
