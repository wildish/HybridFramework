/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.utils;

import android.content.Context;

import com.wildish.hybridframework.core.HybridGlobal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtil {

    public static void unZipFile(String sourcePath, String folderPath) {
        try {
            ZipInputStream inZip = new ZipInputStream(new FileInputStream(sourcePath));
            ZipEntry zipEntry;
            String szName = "";

            while ((zipEntry = inZip.getNextEntry()) != null) {
                szName = zipEntry.getName();
                if (zipEntry.isDirectory()) {
                    szName = szName.substring(0, szName.length() - 1);
                    File folder = new File(folderPath + File.separator + szName);
                    folder.mkdirs();
                    File createNoMedia = new File(folder, ".nomedia");
                    try {
                        createNoMedia.createNewFile();
                    } catch (Exception e) {
                    }
                } else {
                    File file = new File(folderPath + File.separator + szName);
                    if (file.exists() && zipEntry.getTime() == file.lastModified()) {
                        continue;
                    }
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                        File createNoMedia = new File(file.getParentFile(), ".nomedia");
                        try {
                            createNoMedia.createNewFile();
                        } catch (Exception e) {
                        }
                    }
                    if (!file.exists()) {
                        file.createNewFile();
                    } else if (zipEntry.getTime() != file.lastModified()) {
                        file.delete();
                        file.createNewFile();
                    }
                    FileOutputStream out = new FileOutputStream(
                            file);
                    int len;
                    byte[] buffer = new byte[4096];
                    while ((len = inZip.read(buffer)) != -1) {
                        out.write(buffer, 0, len);
                        out.flush();
                    }
                    out.close();
                }
            }
            inZip.close();
        } catch (IOException e) {
            HybridGlobal.logger.log(e);
        }
    }

    public static void unZip(Context context, String path, String uiFileName) {
        File root = new File(path);
        if (!root.exists()) {
            root.mkdirs();
            File createNoMedia = new File(root, ".nomedia");
            try {
                createNoMedia.createNewFile();
            } catch (Exception e) {
                HybridGlobal.logger.log(e);
            }
        }
        unZipFolder(context, uiFileName, path);
    }

    private static void unZipFolder(Context context, String zipFileString, String outPathString) {
        try {
            ZipInputStream inZip = new ZipInputStream(context.getAssets().open(zipFileString));
            ZipEntry zipEntry;
            String szName = "";

            while ((zipEntry = inZip.getNextEntry()) != null) {
                szName = zipEntry.getName();
                if (zipEntry.isDirectory()) {
                    szName = szName.substring(0, szName.length() - 1);
                    File folder = new File(outPathString + File.separator + szName);
                    folder.mkdirs();
                    File createNoMedia = new File(folder, ".nomedia");
                    try {
                        createNoMedia.createNewFile();
                    } catch (Exception e) {
                        HybridGlobal.logger.log(e);
                    }
                } else {
                    File file = new File(outPathString + File.separator + szName);
                    if (file.exists() && zipEntry.getTime() == file.lastModified()) {
                        continue;
                    }
                    if (!file.exists()) {
                        file.createNewFile();
                    } else if (zipEntry.getTime() != file.lastModified()) {
                        file.delete();
                        file.createNewFile();
                    }
                    FileOutputStream out = new FileOutputStream(file);
                    int len;
                    byte[] buffer = new byte[1024];
                    while ((len = inZip.read(buffer)) != -1) {
                        out.write(buffer, 0, len);
                        out.flush();
                    }
                    out.close();
                }
            }
            inZip.close();
        } catch (IOException e) {
            HybridGlobal.logger.log(e);
        }
    }
}
