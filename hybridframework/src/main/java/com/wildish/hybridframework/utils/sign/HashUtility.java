/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */
package com.wildish.hybridframework.utils.sign;

import java.security.MessageDigest;

public final class HashUtility {

	public static String md5Hash(String context) {
		StringBuilder sb = new StringBuilder();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(context.getBytes());
			byte[] digest = md.digest();
			for (int i = 0; i < digest.length; i++) {
				int v = digest[i] & 0xFF;
				String hv = Integer.toHexString(v);
				if (hv.length() < 2) {
					sb.append(0);
				}
				sb.append(hv);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

}
