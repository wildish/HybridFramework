/*
 * Created by Wildish on 2016-11-10
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.proxy;

import android.content.Intent;

public abstract class BaseHybridProxy implements HybridProxy {
    protected String key;
    protected DataHandler dataHandler;

    protected static final String COMMON_FAIL_RESP = "{\"success\": false}";
    protected static final String COMMON_FAIL_NO_SUCH_METHOD = "{\"success\": false, \"msg\": \"未找到指定方法\"}";

    public BaseHybridProxy(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

    public void setDataHandler(DataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }

    public void sendToWebview(String key, String method, String resultStr) {
        HybridProxyManager.getInstance().sendToWebview(key, method, resultStr);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {}

    public void onTakeFocus(){}
}
