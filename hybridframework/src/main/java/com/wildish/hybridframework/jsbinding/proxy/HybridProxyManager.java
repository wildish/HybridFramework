/*
 * Created by Wildish on 2016-11-10
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.proxy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.wildish.hybridframework.core.HybridGlobal;
import com.wildish.hybridframework.jsbinding.webview.JsHelper;

import org.json.JSONObject;

import java.util.HashMap;

public class HybridProxyManager {
    private static HybridProxyManager instance;

    private HybridProxy activityResultProxy;
    private int requestCode;

    private HybridProxy takeFocusProxy;

    private HashMap<String, HybridProxy> proxyHashMap = new HashMap<>();

    public static final String METHOD_KEY = "method_key";

    public static HybridProxyManager getInstance() {
        if(instance == null) {
            instance = new HybridProxyManager();
        }

        return instance;
    }

    public void execute(Context context, String key, String action, JSONObject params) {
        if(proxyHashMap.containsKey(key)) {
            HybridProxy proxy = proxyHashMap.get(key);
            proxy.execute(context, action, params);
        }
    }

    public synchronized void addProxy(HybridProxy proxy) {
        String key = proxy.getKey();
        if(proxyHashMap.containsKey(key)) {
            proxy = proxyHashMap.get(key);
            proxy.release();
        }
        proxyHashMap.put(key, proxy);
    }

    public synchronized void removeProxy(String key, HybridProxy proxy) {
        if(proxyHashMap.containsKey(key)) {
            proxy = proxyHashMap.get(key);
            proxy.release();
        }
        proxyHashMap.remove(key);
    }

    public void setActivityResultProxy(HybridProxy proxy) {
        if(activityResultProxy != null) {
            activityResultProxy.onActivityResult(requestCode, Activity.RESULT_CANCELED, null);
        }
        activityResultProxy = proxy;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(activityResultProxy != null) {
            activityResultProxy.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void startActivityForResult(HybridProxy proxy, Intent intent, int requestCode) {
        this.requestCode = requestCode;
        setActivityResultProxy(proxy);

        if(HybridGlobal.getCurrentWebView() != null && HybridGlobal.getCurrentWebViewContext() != null) {
            HybridGlobal.getCurrentWebViewContext().startActivityForResult(intent, requestCode);
        }
    }

    public void onTakeFocus() {
        if(this.takeFocusProxy != null) {
            this.takeFocusProxy.onTakeFocus();
        }
    }

    public void setOnTakeFocusProxy(HybridProxy proxy) {
        this.takeFocusProxy = proxy;
    }

    public void startActivity(HybridProxy proxy, Intent intent) {
        if(proxy != null) {
            setOnTakeFocusProxy(proxy);
        }
        if(HybridGlobal.getCurrentWebView() != null && HybridGlobal.getCurrentWebViewContext() != null) {
            HybridGlobal.getCurrentWebViewContext().startActivity(intent);
        }
    }

    public void sendToWebview(String proxy, String method, String data) {
        HybridGlobal.getCurrentWebView().loadUrl(JsHelper.createCallback(proxy, method, data));
    }

    public HybridProxy getDefaultProxy() {
        return proxyHashMap.get(HybridGlobal.PROXY_HF_DEFAULT);
    }
}
