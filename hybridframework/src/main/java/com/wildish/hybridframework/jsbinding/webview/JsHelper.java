/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.webview;

import android.webkit.WebView;

import org.json.JSONObject;

import java.util.HashMap;

public class JsHelper {
    private static HashMap<String, WebView> webViewMap = new HashMap<>();

    public static void register(String key, WebView webView) {
        if(webViewMap.containsKey(key)) {
            webViewMap.remove(key);
        }
        webViewMap.put(key, webView);
    }

    public static void remove(String key) {
        webViewMap.remove(key);
    }

    public static WebView get(String key) {
        return webViewMap.get(key);
    }

    public static String createCallback(String key, String jsonStr) {
        return "javascript:(typeof $win != \"undefined\")?$win.jsCallback(\""+key+"\", " + jsonStr + "):\"\"";
    }

    protected JSONObject parameter;
    public void setParameter(JSONObject parameter) {
        this.parameter = parameter;
    }

    public static String createCallback(String proxy, String method, String jsonStr) {
        return "javascript:(typeof $win != \"undefined\")?$win.callbackProxy(\""+proxy+"\",\""+method+"\", "+jsonStr+"):\"\"";
    }
}
