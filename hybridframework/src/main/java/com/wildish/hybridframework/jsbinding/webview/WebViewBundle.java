/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.webview;

import android.os.Bundle;

public class WebViewBundle {
    public static final String BUNDLE_PARAMS = "BUNDLE_PARAMS";
    private Bundle bundle = new Bundle();
    private static WebViewBundle instance;

    public static WebViewBundle getInstance() {
        if(instance == null) {
            instance = new WebViewBundle();
        }
        return instance;
    }

    public void setBundle(Bundle bundle) {
        this.bundle.clear();
        this.bundle = bundle;
    }

    public void setBundle(String paramsStr) {
        this.bundle.clear();
        this.bundle.putString(BUNDLE_PARAMS, paramsStr);
    }

    public Bundle getBundle() {
        return this.bundle;
    }

    public void clearBundle() {
        this.bundle.clear();
    }
}
