/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.webview;

import android.app.Activity;
import android.webkit.WebView;

public class WebViewPage {
    private Activity context;
    private WebView webView;
    private String webViewTag;

    public WebViewPage(Activity context, WebView webView, String webViewTag) {
        this.context = context;
        this.webView = webView;
        this.webViewTag = webViewTag;
    }

    public Activity getContext() {
        return context;
    }

    public WebView getWebView() {
        return webView;
    }

    public String getWebViewTag() {
        return webViewTag;
    }
}
