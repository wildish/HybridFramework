/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.webview;

import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.JavascriptInterface;

import com.wildish.hybridframework.core.HybridGlobal;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * Created by Keith on 2016/3/18.
 */
public class HybridJsGlobal extends JsHelper {

    private Context context;

    public HybridJsGlobal() {}

    public HybridJsGlobal(Context context) {
        this.context = context;
    }

    @JavascriptInterface
    public void log(String message) {
        HybridGlobal.logger.log(message);
    }

    @JavascriptInterface
    public void setCookie(String key, String message) {
        SharedPreferences sp = HybridGlobal.getInstance().getGlobalSp();
        if(sp.contains(key)) {
            sp.edit().remove(key).apply();
        }
        sp.edit().putString(key, message).apply();
    }

    @JavascriptInterface
    public String getCookie(String key) {
        return HybridGlobal.getInstance().getGlobalSp().getString(key, "");
    }

    @JavascriptInterface
    public String initSystemConfig() {
        String autoPlay = StringUtils.isBlank(getCookie("auto_play"))?"1":getCookie("auto_play");
        return "{\"autoplay\": " + autoPlay +"}";
    }

    @JavascriptInterface
    public String getVersionInfo() {
        return HybridGlobal.getInstance().getVersionInfo();
    }

    @JavascriptInterface
    public String getCustomScheme() {
        return HybridGlobal.getCustomScheme();
    }
}
