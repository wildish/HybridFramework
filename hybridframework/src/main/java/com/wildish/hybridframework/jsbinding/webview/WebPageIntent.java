/*
 * Created by Wildish on 2016-10-31
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.webview;

import org.json.JSONObject;

public class WebPageIntent {
    public static final String PAGE_TYPE_OUTLINK = "TYPE_OUTLINK";    // 外部链接
    public static final String PAGE_TYPE_INNERLINK = "TYPE_INNERLINK";    // 内部链接，通过project参数、page参数和paramsdata(JSON字符串)参数加载页面
    public static final String PAGE_TYPE_DEFAULT = "TYPE_DEFAULT";    // 内部链接，通过project参数访问该项目config.js中配置的页面

    private String page;
    private String project;
    private String moduleName;
    private JSONObject params;
    private String pageType;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }
}
