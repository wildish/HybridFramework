/*
 * Created by Wildish on 2016-11-16
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.proxy;

import org.json.JSONObject;

public interface DataHandler {
    String COMMON_FAIL_RESP = "{\"success\": false}";
    String COMMON_FAIL_NO_HANDLER = "{\"success\": false, \"msg\": \"No DataHandler implements.\"}";

    String getData(JSONObject parameter);
}
