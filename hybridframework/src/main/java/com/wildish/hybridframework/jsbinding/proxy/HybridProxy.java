/*
 * Created by Wildish on 2016-11-10
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hybridframework.jsbinding.proxy;

import android.content.Context;
import android.content.Intent;

import org.json.JSONObject;

public interface HybridProxy {
    String getKey();

    void release();

    void execute(Context context, String action, JSONObject params);

    void setDataHandler(DataHandler dataHandler);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onTakeFocus();
}
