package com.chinark.apppickimagev3.utils;

import java.util.ArrayList;

/**
 * Created by Keith on 2016/8/15.
 */
public class ImagePickResult {
    private ArrayList<String> imageList = new ArrayList<>();

    private static ImagePickResult instance;

    public static ImagePickResult getInstance() {
        if(instance == null) {
            instance = new ImagePickResult();
        }
        return instance;
    }

    public void setImageList(ArrayList<String> list) {
        imageList = list;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void clearImageList() {
        imageList.clear();
    }
}
