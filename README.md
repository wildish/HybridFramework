#HybridFramework

开发Hybrid App的Html容器，基于自定义私有协议实现，同前端开发环境 [NodeWeb](http://git.oschina.net/wildish/NodeWeb)协同工作
当然，如果习惯在android studio中编写html代码的话，也是可以直接使用的

##写在前面
笔者并没有奢望能创造出来一个能跟cordova一较高下的框架，如果从头开始的话，笔者更愿意从一开始就使用cordova
只是希望能从工作经验出发，维护一个小型项目使用的，可以快速开发、测试的版本，并对自己的工作做一些总结

##基本原理
HybridFramework同常规HybridApp的区别主要体现在
web页面通过Proxy同android native交互，Proxy通过私有协议实现，而不是使用JavascriptInterface。
    好处是前端代码兼容iOS(**目前iOS框架基本功能已有但尚未重构，有兴趣的可以联系我**)

##基本使用
**V0.2**
直接导入项目到AS，即可开始使用。

**V0.1**
配置gradle
如果同NodeWeb协同工作，需要配置此步
1. 打开app/build.gradle，在"开发配置"模块会看到三个任务copyWeb, zipWeb, cleanWeb和一个参数nodePath
2. 配置nodePath到本地NodeWeb项目根目录下的web目录
3. 打开copyWeb, zipWeb, cleanWeb的注释
即可完成配置。
该配置的作用是将NodeWeb/web目录复制到assets目录下，并压缩成web.data，并删除assets/web目录
如果您希望直接在android studio中编写html文件并运行调试，
则保持当前app/build.gradle中"开发配置"模块不变。
现在就可以直接通过android studio运行，并使用模拟器或者真机看到结果了

##TODO
- 离线资源版本管理和更新机制(2016-11-18)
- 使用可管理的离线资源系统，优化在线网页访问(2016-11-18)
- 网络页面同内部页面交互，以及网络页面的跨域操纵(2016-11-18)
- 对于集成测试的支持(2016-11-18)
- 前端框架修改(2016-11-18)
    - 样式使用em而不是px，注意android和iOS系统的区别(2016-11-18)
    - 在目前initDom, initData, initEvent基础上，加入on pause和on resume等状态停止和恢复的支持，插件也应该实现相关接口，在系统状态变化时做出相应的处理(2016-11-29)
    - 展示内容的国际化，使用jquery.i18n.properties，纯前端实现(2016-11-30)
    
##修改记录
####2017-02-06
前段时间出差和过年，今年计划使用Vue重写[NodeWeb](http://git.oschina.net/wildish/NodeWeb)，并逐步实现TODO-list中的内容。另外还有一些脑洞，整理之后再写吧

####2016-11-18
今天看到了一个 [关于Hybrid讨论的帖子](https://github.com/chemdemo/chemdemo.github.io/issues/12) 
里面对Hybrid模式进行了不错的总结，里面提到了一个很好的关于资源离线缓存的问题，主要的应用场景就是在访问在线资源时，通过webview的转发，访问本地资源从而达到节省流量提高速度的目的
结合目前框架的实现，需要在自定义webview(CustomWebClient.java)上做一些工作，通过HybridGlobal对外暴露相关接口
其实这个部分本来也是在计划中的，但是考虑到本框架的主要应用场景并不是为了访问在线网页而存在的，最后就把这部分的工作先放下了。

在回复中还提到了一个开源框架 [kerkee](http://www.kerkee.com/)~~果然还是重复制造轮子了么555555~~，很值得学习。
不过kerkee使用的GPLv3协议，对商用不友好，请大家注意。

另外，关于自动化测试部分，笔者并不知道对于HybridApp如何集成测试框架或者有什么测试框架可以直接使用的，不过看到kerkee文档中提到了测试的相关内容，有时间了学习一下
目前该框架的集成测试部分是在[NodeWeb](http://git.oschina.net/wildish/NodeWeb)中进行的，基于NodeJS，提供了HttpServer可以模拟数据请求，可以使用集成测试，可以在PC上调试页面，可以通过少量的配置无缝引入本框架，感觉暂时已经够用了

**怎么说呢，笔者已经不想在HybridApp模式上花费更多的时间了，对于Hybrid的常见模式，本框架和上面的帖子链接里面已经做了不少。
笔者会维护一个TODO-List，对于能想到的问题进行记录，如果以后碰到实际应用中需求并实现了，会更新到版本中。
同时也欢迎PR。**

####2016-11-17
版本更新到V0.2。作为当前以及以后的稳定版本，实现了初步的模块化，并补充了相关的样例。
V0.2相比于V0.1最大的变化是去掉了内置了Build-In HttpServer，提出了proxy的概念替代原有的HttpHandler，用于前端js同本地代码的交互。
框架提供了代理的自定义接口，方便进行扩展。

剩余的一个问题是在不更新程序的情况下动态更新内容，这包括两个方面的内容
1. 动态更新Proxy。目前笔者没有做出太好的尝试。
曾经实现过动态加载assets中的APK并从中动态加载类并运行，但是考虑到实现复杂度和实际应用中的情况，最好的解决方案还是更新程序
2. 动态更新assets中的文件并展示
这应该是一个很现实的需求，但是问题是assets目录并不能通过文件系统访问或者操作，那么实现思路就变成了
- assets中保存一个web文件包和一个版本文件，启动程序时，将web文件包解压到android文件目录中，并保存web根目录；
- 页面加载时根据2.1保存的web根目录从android文件系统中加载文件（其实就是修改config.js中的this.server字段就可以了）
- 框架提供
        ·获取/更新当前web版本号
        ·更新文件系统
    的方法，并需要用户实现
        ·更新状态回调
    接口的方式，实现检查提示更新的功能

cordova可以通过 [cordova-app-loader](https://github.com/markmarijnissen/cordova-app-loader) 插件实现类似的功能
它好像维护了一个更详细的manifest.json用于维护文件版本，没有仔细看，估计原理跟这思路差不多。
**大体的思路就这样了，框架不再提供具体实现。**

####2016-11-10
版本更新到V0.1。作为第一个版本，已经对原有框架进行了初步重构，实现HybridApp的常用功能。

####2016-11-09
同步NodeWeb代码
修改日志，加入日志缓冲区，优化日志输出
系统的日志系统只用于调试和输出到文件，如果要在正式发布的应用中追踪异常，推荐使用**腾讯bugly**

####2016-11-04
初始化上传
目前还没有文档，稍后会补充完整使用说明
最近事情会比较多，脑洞也比较大，希望能对整个系统进行重构，主要是针对模块解耦，动态加载部分
我想等重构得差不多了，再补充完整的文档


##
能力有限，如果您看到了这个项目并有任何想法，忘不吝赐教
keithknight@qq.com