package com.wildish.hfdemo;

import android.app.Application;

import com.wildish.hfdemo.handler.CommonRequestHandler;
import com.wildish.hfdemo.proxy.alipay.AlipayProxy;
import com.wildish.hfdemo.utils.Global;
import com.wildish.hybridframework.core.HybridGlobal;
import com.wildish.hybridframework.utils.Logger;

public class DemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // 初始化HybirdFramework运行环境
        HybridGlobal.getInstance().initHybridGlobal(getApplicationContext(), Global.BUILD_IN_SCHEME);
        // 初始化HybirdFramework logger功能
        HybridGlobal.logger.setTarget(Logger.Debug, Logger.Fatal);
        // 添加自定义代理
        HybridGlobal.getInstance().addProxy(new AlipayProxy(Global.PROXY_ALIPAY));
        // 实现自定义数据处理
        HybridGlobal.getInstance().setDataHandler(new CommonRequestHandler());
    }
}
