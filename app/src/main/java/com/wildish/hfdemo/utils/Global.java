package com.wildish.hfdemo.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Global {
    public static final ExecutorService pool = Executors.newFixedThreadPool(5);
    public static final String PROJECT_NAME = "demo";

    // true表示访问DEBUG_SERVER_URL, false表示访问SERVER_URL
    public static final boolean DEBUG = true;
    public static final String DEBUG_SERVER_URL = "http://192.168.18.236:3000";
    public static final String SERVER_URL = "";
    public static final String BUILD_IN_SCHEME = "wildish";
    public static final String SERVER_NOTIFY_URL = "http://www.baidu.com/alipay";

    public static final String PROXY_ALIPAY = "proxy_alipay";
    public static boolean isInit = false;
}
