package com.wildish.hfdemo.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import com.wildish.hfdemo.R;
import com.wildish.hfdemo.utils.Global;
import com.wildish.hybridframework.core.HybridGlobal;
import com.wildish.hybridframework.jsbinding.webview.WebPageIntent;

public class SplashActivity extends Activity {
    private TextView tvCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tvCheck = (TextView) findViewById(R.id.tv_check);

        Global.pool.submit(new Runnable() {
            @Override
            public void run() {
                if(!Global.isInit) {
                    // 这里非常适合做一些耗时操作
                    // 比如从服务器读取一些初始化数据，检查是否有版本或者数据更新等
                    msgHandler.sendEmptyMessage(0);
                } else {
                    msgHandler.sendEmptyMessage(1);
                }
            }
        });
    }

    private Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Global.isInit = true;
                    tvCheck.setText("初始化成功");
                    WebPageIntent intent = new WebPageIntent();
                    intent.setProject(Global.PROJECT_NAME);
                    intent.setPageType(WebPageIntent.PAGE_TYPE_DEFAULT);
                    HybridGlobal.showPage(SplashActivity.this, intent);
                    break;
                case 1:
                    tvCheck.setText("欢迎重新进入系统");
                    break;
            }
            SplashActivity.this.finish();
        }
    };
}
