/*
 * Created by Wildish on 2016-11-17
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 */

package com.wildish.hfdemo.proxy.alipay;

import com.wildish.hybridframework.utils.sign.Base64;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

/**
 * Created by Keith on 2016/6/15.
 */
public class AlipayUtil {
    public static final String PARTNER = "2088121216596780";
    // 商户收款账号
    public static final String SELLER = "scbd@scbdlbs.com";
    // 商户私钥，pkcs8格式
    public static final String RSA_PRIVATE = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBALp1QmUXJ85lFRa7OR3z6x1rTrStO7veC/gpm7kjMgDaqKK+0A1dmf4DYxuaQl8FJbBFpJW5gX3GDzehLc3uhHOosmMrduapXrdMB4XLXXd6IswGKZti2ub8BS2Ky66YTg7RpVp13Hpf2tAQwJZjlcrkY/AylvcIBGovy81+8lcpAgMBAAECgYB1tl1qMIoi1NHRffT7IehGtF4F1m+llPL9glcbNEr3GxPnWotCRNtY7457+JD6J+e5Nt5G38wOOSGWPx0JqxDWV7Z8T1yrVPnI+0HlspIGxhr11AhH1+7dyuW4WcvLHFCNIVTIA49s8j4w6sHWRiuTOPBrScXCXR860nl+pouHYQJBAN9i0k4haiZ4Z5vRdjIzg3EqY+EifzlaGVqsDzfsoHGIbssl1gDEZeQpG4N41Yq494ES6fLiIR3TNcIFiJv8GyUCQQDVrjvf47UBSkndm/y8a/cUFayFtl1KPidTPZnta7pPiXyFv/6rhb5Rk0Sm53H+e7k2OXk5GqSrZ4TH76sqQK61AkEA3s2b4N2nGFPDU0Cp2QxUL40OLcDQkA1zeGJgk3Dp3hMsHmbQeddpoRX3ITxmEITIJ+8LdHtieHLvDWOji6D6GQJBAIj+oNKE6co+96aF2H5w9cLBto8vgRYVTR8YEnTeKV/O9K/HjR4orAEe2tfq8PHIHtF9/ZM32rUttDp/FQ0M1TUCQQCulPnbSQxUpQX6GeX1J4sAB11qBiUxot3yp0GRHM2eNsjHFV2/MY6iCjwVvIKw/vu0xsEdSeCMJQwiwOtYnwBF";

    public static String generateOrder(String traderNo, String subject, String body, String totalFee, String notifyUrl) {
        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + traderNo + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + totalFee + "\"";

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + notifyUrl + "\"";

        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        orderInfo += "&it_b_pay=\"30m\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";
        return orderInfo;
    }

    private static final String ALGORITHM = "RSA";
    private static final String SIGN_ALGORITHMS = "SHA1WithRSA";
    private static final String DEFAULT_CHARSET = "UTF-8";

    public static String sign(String content) {
        return sign(content, RSA_PRIVATE);
    }

    private static String sign(String content, String privateKey) {
        try {
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(Base64.decode(privateKey));
            KeyFactory keyf = KeyFactory.getInstance(ALGORITHM);
            PrivateKey priKey = keyf.generatePrivate(priPKCS8);

            java.security.Signature signature = java.security.Signature
                    .getInstance(SIGN_ALGORITHMS);

            signature.initSign(priKey);
            signature.update(content.getBytes(DEFAULT_CHARSET));

            byte[] signed = signature.sign();

            return Base64.encode(signed);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
