/**
 * waiting实现waiting功能
 *  使用：
 *  	Waiting.show();
 *  	Waiting.hide();
 */
define(['jquery'], function() {
	function Waiting() {
		this.root = $("<div class=\"bg_popup\"></div>");
		this.root.append("<div class='loader-waiting-2'></div>");
		this.count = 0;
		this.initHtml();
	}

	Waiting.prototype.initHtml = function() {
		var html = "<div class='loader-inner ball-spin-fade-loader'><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>";
		this.root.append(html);
		$("body").append(this.root);
		this.hide();
	};

	Waiting.prototype.show = function(msg) {
		if(this.count == 0) {
		    msg = msg?msg:"";
			this.showmsg(msg);
			this.root.show();
			this.count++;
		} else {
			this.count++;
		}
	};

	Waiting.prototype.showmsg = function(msg) {
		if($("#wait_showmsg").length != 0) {
			return;
		}
		var _this = this;
		var msgView = $("<div id='wait_showmsg' style=''></div>");
		msgView.text(msg);
		msgView.css({
			"position": "absolute",
			"top": "0",
			"left": "0",
			"line-height": $(window).height() + "px",
			"text-align": "center",
			"width": "100%",
			"height": $(window).height() + "px",
			"color": "#fff",
//			"background-color": "rgba(0, 0, 0, 0.4)" ,
			"background-color": "transparent" ,
			"font-size:":"18px"
		});
		$("body").append(msgView);
	};


	Waiting.prototype.hide = function() {
		this.count--;
		if(this.count < 0 ) {
			this.count = 0;
		}
		if(this.count == 0) {
			$("#wait_showmsg").remove();
			this.root.hide();
		}
	};

	window.Waiting = new Waiting();
});
