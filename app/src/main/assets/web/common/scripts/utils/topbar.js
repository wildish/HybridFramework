/*
实现功能
    页面头部标题，返回，右侧菜单等组合的topbar,在$win的基础上实现，如不使用$win，需使用new重新获取对象
使用规则
eg:
    $win.topbar.hide();
    $win.topbar.show();
    $win.topbar.setTitle("我是标题");
    $win.topbar.addBackward();         -- 在topbar左侧添加返回按钮。
    $win.topbar.removeItem(key);       --根据key删除某一item
    $win.topbar.addTextMenuItem(key, pos, text, callback);
        --key ：为该item添加识别可识别的唯一key
        --pos : 分为 Topbar.MENU_LEFT 和 Topbar.MENU_RIGHT
        --text ： 文字信息
        --callback : 点击触发事件
    $win.topbar.removeAllItem();  -- 删除所有的item
    $win.topbar.initCommon();   --初始化topbar
*/
/*global $win define:true*/
define(['jquery', 'shoppingcartItem'], function($, shoppingcartItem) {
    function Topbar(id, title) {
        this.id = id;
        this.title = (typeof title == "undefined") ? "" : title;
        this.$wrapper = null;
        this.$base = null;
        this.$title = null;
        this._$leftMenuContainer = null;
        this._$rightMenuContainer = null;
        this._visible = true;

        this._menuMap = {};

        this._init();
    }
    Topbar.KEY_BACKHOME = "key_backhome";
    Topbar.KEY_BACKWARD = "key_backward";
    Topbar.KEY_MENU = "key_menu";
    Topbar.KEY_COMMIT = "key_commit";
    Topbar.KEY_ADDRESSADD = "key_addressadd";
    Topbar.KEY_SPOTFOOT = "key_spotfoot";
    Topbar.KEY_GUIDELIST = "key_guidelist";
    Topbar.KEY_FOOTSHARE = "key_footshare";
    Topbar.KEY_FOOTLIKE = "key_footlike";
    Topbar.KEY_FOOTDISLIKE = "key_footdislike";
    Topbar.KEY_GUIDEMAP = "key_guidemap";
    Topbar.KEY_SECNICSEARCH = "key_scenicsearch";
    Topbar.KEY_CUSTOMBACKWARD = "key_custombackward";
    Topbar.KEY_DELADDRESS = "key_deladdress";
    Topbar.MENU_LEFT = 0;
    Topbar.MENU_RIGHT = 1;
    Topbar.CLASS_BACKWARD = "menu-backward";
    Topbar.CLASS_MENU = "menu-oper";

    Topbar.prototype._init = function() {
        this.$wrapper = $("#" + this.id);
        this.$wrapper.addClass("header-wrapper");
        this.$base = $("<div class=\"header\"></div>");
        this.$wrapper.append(this.$base);
        this.$leftMenu = $("<div class='head-menu fl'></div>");
        this.$title = $("<div class='head-title fl'>" + this.title + "</div>");
        this.$rightMenu = $("<div class='head-menu fr'></div>");
        this.$base.append(this.$leftMenu)
            .append(this.$title)
            .append(this.$rightMenu);
    };

    Topbar.prototype.setTitle = function(title) {
        this.title = title;
        this.$title.html(title);
    };

    Topbar.prototype.hide = function() {
        this._visible = false;
        this.$wrapper.hide();
    };

    Topbar.prototype.show = function() {
        this._visible = true;
        this.$wrapper.show();
    };

    Topbar.prototype.getHeight = function() {
        return this.$wrapper.height();
    };

    Topbar.prototype.isVisible = function() {
        return this._visible;
    };

    Topbar.prototype.addBackward = function() {
        this.addMenuItem(Topbar.KEY_BACKWARD, Topbar.MENU_LEFT, "backward", function() {
            $win.backward();
        });
    };

    Topbar.prototype.addCustomBackward = function(callback) {
        this.addMenuItem(Topbar.KEY_CUSTOMBACKWARD, Topbar.MENU_LEFT, "backward", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addCancel = function() {
        this.addMenuItem(Topbar.KEY_BACKHOME, Topbar.MENU_LEFT, "backward", function() {
            $win.cancel();
        });
    };

    Topbar.prototype.addShoppingcartItem = function(id,image, callback, refresh) {
        var shoppcartItem = new shoppingcartItem({
            id: id,
            image:image,
            onClick: callback,
            onRefresh: refresh
        });
        shoppcartItem.$base.addClass("header-item fr");
        this.$rightMenu.append(shoppcartItem.$base);
        this._menuMap[id] = shoppcartItem;
    };

    Topbar.prototype.getShoppingcartItem = function(key) {
        if (this._menuMap.hasOwnProperty(key)) {
            var dom = this._menuMap[key];
            return dom;
        }
        return null;
    };

    Topbar.prototype.addAddressAddBtn = function(callback) {
        this.addMenuItem(Topbar.KEY_ADDRESSADD, Topbar.MENU_RIGHT, "address-add-btn", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperMenu = function() {
        this.addMenuItem(Topbar.KEY_MENU, Topbar.MENU_RIGHT, "menuoper", function() {
            // TODO
        });
    };

    Topbar.prototype.addOperScenicSearch = function(callback) {
        this.addMenuItem(Topbar.KEY_SECNICSEARCH, Topbar.MENU_RIGHT, "scenicsearch-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperFootShare = function(callback) {
        this.addMenuItem(Topbar.KEY_FOOTSHARE, Topbar.MENU_RIGHT, "footshare-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperFootLike = function(callback) {
        this.addMenuItem(Topbar.KEY_FOOTLIKE, Topbar.MENU_RIGHT, "footlike-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperFootDisLike = function(callback) {
        this.addMenuItem(Topbar.KEY_FOOTDISLIKE, Topbar.MENU_RIGHT, "footdislike-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperSpotFoot = function(callback) {
        this.addMenuItem(Topbar.KEY_SPOTFOOT, Topbar.MENU_RIGHT, "spotfoot-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperGuideMap = function(callback) {
        this.addMenuItem(Topbar.KEY_GUIDEMAP, Topbar.MENU_RIGHT, "guidemap-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addOperGuideList = function(callback) {
        this.addMenuItem(Topbar.KEY_GUIDELIST, Topbar.MENU_RIGHT, "guidelist-menu", function() {
            callback.call(this);
        });
    };

    Topbar.prototype.addMenuItem = function(key, pos, style, callback) {
        if (this._menuMap.hasOwnProperty(key)) {
            this.removeItem(key);
        }
        var $menuItem = $("<div class='header-item'></div>");
        $menuItem.addClass(style);
        switch (pos) {
            case Topbar.MENU_LEFT:
                $menuItem.addClass("fl");
                this.$leftMenu.append($menuItem);
                break;
            case Topbar.MENU_RIGHT:
                $menuItem.addClass("fr");
                this.$rightMenu.append($menuItem);
                break;
        }
        $menuItem.click(callback);

        this._menuMap[key] = $menuItem;
    };

    Topbar.prototype.removeItem = function(key) {
        if (this._menuMap.hasOwnProperty(key)) {
            var dom = this._menuMap[key];
            if (dom.$base != undefined && dom.$base != null) {
                dom.$base.remove();
            } else {
                dom.remove();
            }
            delete this._menuMap[key];
        }
    };

    Topbar.prototype.addTextStyleMenuItem = function(key, pos, text, customStyle, callback) {
        var $menuItem = $("<div class='header-text-item' style='" + customStyle + "'><span style='margin-left:.6rem'>" + text + "</span></div>");
        switch (pos) {
            case Topbar.MENU_LEFT:
                $menuItem.addClass("fl");
                this.$leftMenu.append($menuItem);
                break;
            case Topbar.MENU_RIGHT:
                $menuItem.addClass("fr");
                this.$rightMenu.append($menuItem);
                break;
        }
        $menuItem.bindClick(callback);

        this._menuMap[key] = $menuItem;
    };

    Topbar.prototype.addImgMenuItem = function(key, pos, imgUrl, callback) {
        var $menuItem = $("<div class='header-text-item'><img src='" + imgUrl + "'</div>");
        switch (pos) {
            case Topbar.MENU_LEFT:
                $menuItem.addClass("fl");
                this.$leftMenu.append($menuItem);
                break;
            case Topbar.MENU_RIGHT:
                $menuItem.addClass("fr");
                this.$rightMenu.append($menuItem);
                break;
        }
        $menuItem.bindClick(callback);

        this._menuMap[key] = $menuItem;
    };

    Topbar.prototype.addTextMenuItem = function(key, pos, text, callback) {
        var $menuItem = $("<div class='header-text-item'>" + text + "</div>");
        switch (pos) {
            case Topbar.MENU_LEFT:
                $menuItem.addClass("fl");
                this.$leftMenu.append($menuItem);
                break;
            case Topbar.MENU_RIGHT:
                $menuItem.addClass("fr");
                this.$rightMenu.append($menuItem);
                break;
        }
        $menuItem.bindClick(callback);

        this._menuMap[key] = $menuItem;
    };

    // 删除所有top item
    Topbar.prototype.removeAllItem = function() {
        for (var key in this._menuMap) {
            var dom = this._menuMap[key];
            if (dom.$base != undefined && dom.$base != null) {
                dom.$base.remove();
            } else {
                dom.remove();
            }
            delete this._menuMap[key];
        }
    };

    Topbar.prototype.initCommon = function() {
        this.removeAllItem();
        this.addBackward();
        this.setTitle("");
        this.hide();
    };

    return Topbar;
});
