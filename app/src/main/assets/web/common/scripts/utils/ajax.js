/**
 * simple version
 * instead of encrypting params in javascript, do it in android local server
 */
/*global define Waiting:true*/
define(['jquery'], function($) {
    $.ajaxSetup({
        type: "POST",
        contentType: "text/plain",
        async: true,
        processdata: false,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log(errorThrown.stack);
            if (textStatus == "timeout") {
                // handle timeout
                //debugger;
            } else {
                // handle unknow error
                //debugger;
            }
            if (Waiting && Waiting.hide) {
                Waiting.hide();
            }
        }
    });

    // 不使用加密
    $.post = function(url, data, success, showWaiting) {
        if (typeof showWaiting == "undefined") {
            showWaiting = false;
        }

        if (Waiting && Waiting.show && showWaiting) {
            Waiting.show();
        }
        var options = {
            url: url,
            data: encodeURIComponent(JSON.stringify(data)),
            success: function(data) {
                if (Waiting && Waiting.hide && showWaiting) {
                    Waiting.hide();
                }
                success(data);
            },
            dataType: 'json'
        };
        return $.ajax(options);
    };

    /**
     * 九洲公车项目专用
     */
    $.postWithVerify = function(url, data, success, verifyFailed, showWaiting) {
        if (typeof showWaiting == "undefined") {
            showWaiting = true;
        }

        if (Waiting && Waiting.show && showWaiting) {
            Waiting.show();
        }
        var options = {
            url: url,
            data: encodeURIComponent(JSON.stringify(data)),
            success: function(data) {
                if (Waiting && Waiting.hide && showWaiting) {
                    Waiting.hide();
                }
                if(!data.Ret && data.Code == "401"){
                    verifyFailed();
                } else {
                    success(data);
                }
            },
            dataType: 'json'
        };
        return $.ajax(options);
    };

    $.get = function(url, data, success, showWaiting) {
        if (typeof showWaiting == "undefined") {
            showWaiting = true;
        }
        if (Waiting && Waiting.show && showWaiting) {
            Waiting.show();
        }
        var async = true;
        if (typeof data.async != "undefined") {
            async = data.async;
        }
        var options = {
            async: async,
            url: url,
            data: encodeURIComponent(JSON.stringify(data)),
            success: function(data) {
                if (Waiting && Waiting.hide && showWaiting) {
                    Waiting.hide();
                }
                success(data);
            }
        };
        return $.ajax(options);
    };

    $.getasync = function(url, data, success) {
        if (Waiting && Waiting.show) {
            Waiting.show();
        }
        var options = {
            async: true,
            url: url,
            data: encodeURIComponent(JSON.stringify(data)),
            success: function(data) {
                if (Waiting && Waiting.hide) {
                    Waiting.hide();
                }
                success(data);
            }
        };
        return $.ajax(options);
    };


    var kk = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=";
    $.b = function(s, k) {
        var r = [];
        var idx = 0,
            c1, c2, c3, kc;
        while (idx + 2 < s.length) {
            c1 = 0x00FF & s.charCodeAt(idx++);
            c2 = 0x00FF & s.charCodeAt(idx++);
            c3 = 0x00FF & s.charCodeAt(idx++);
            kc = (c1 << 16) + (c2 << 8) + c3 + k;
            r.push(kk[kc & 0x3F]);
            r.push(kk[(kc >> 6) & 0x3F]);
            r.push(kk[(kc >> 12) & 0x3F]);
            r.push(kk[(kc >> 18) & 0x3F]);
        }
        if (idx + 1 == s.length) {
            kc = (s.charCodeAt(idx) << 16) + k;
            r.push(kk[kc & 0x3F]);
            r.push(kk[(kc >> 6) & 0x3F]);
            r.push(kk[(kc >> 12) & 0x3F]);
            r.push(kk[(kc >> 18) & 0x3F]);
        } else if (idx + 2 == s.length) {
            kc = (s.charCodeAt(idx) << 16) +
                (s.charCodeAt(idx + 1) << 8) + k;
            r.push(kk[kc & 0x3F]);
            r.push(kk[(kc >> 6) & 0x3F]);
            r.push(kk[(kc >> 12) & 0x3F]);
            r.push(kk[(kc >> 18) & 0x3F]);
        }
        return r.join("");
    };

    /** 使用加密
    $.ab = function(pa, pb) {
    	var va = "", vb = "", vk = window.k || 0, vp = pa.indexOf('?');
    	if (vp > 0) {
    		va = pa.substring(0, vp), vb = pa.substring(vp);
    		if (pb)
    			vb += "&" + pb;
    	} else {
    		va = pa, vb = "?" + pb;
    	}

    	return {
    		a : va,
    		b : $.b(vb, vk)
    	};
    };

    $.post = function(url, data, success, dataType) {
    	if(Waiting && Waiting.show) {
    		Waiting.show();
    	}
    	var ab = $.ab(url, $.json2str(data));
    	var options = {
    		url : ab.a,
    		data : ab.b,
    		success : function(data) {
    			if(Waiting && Waiting.hide) {
    				Waiting.hide();
    			}
    			success(data);
    		},
    		dataType : dataType,
    	};
    	return $.ajax(options);
    };
    **/

    $.json2str = function(params) {
        var kv = "";
        if (typeof(params) != "string") {
            for (var i in params) {
                kv += encodeURIComponent(i) + "=" +
                    encodeURIComponent(params[i]) + "&";
            }
        } else {
            kv = params;
        }
        return kv;
    };
});
