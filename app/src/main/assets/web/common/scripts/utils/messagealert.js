/**
 * messagealert实现自定义alert功能，分为success，info，warning，error四个类别，除error提示需手动关闭，其它提示2秒钟后自动消失
 * 参数：
 * 		type: success,info,warning,error
 * 		content： 提示内容
 * 		timer: alert显示的时间（毫秒）
 *
 * 使用：
 * 	$.MessageAlert({type: "success", content: "更新成功", timer: 2000});
 */
define(['jquery'], function($) {
    function MessageAlert(opt) {
        this.root = $("<div id=\"messagebox\" class=\"bg_popup " + opt.type + "\"></div>");
        this.root.append("<div class='loader-waiting'></div>");
        opt = opt || {};
        this.type = opt.type || 'info';
        this.content = opt.content || "";
        this.timer = opt.timer || 2000;
        this.initHtml();
    };

    MessageAlert.prototype.initHtml = function() {
        var typeClass = "";
        var typeTitle = "";
        if (this.type == "success") {
            typeClass = "alert-success";
            typeTitle = "成功";
            this.root.css({
                'background': 'transparent',
                'pointer-events': 'none'
            });
        } else if (this.type == "info") {
            typeClass = "alert-info";
            typeTitle = "提示";
            this.root.css({
                'background': 'transparent',
                'pointer-events': 'none'
            });
        } else if (this.type == "warning") {
            typeClass = "alert-warning";
            typeTitle = "警告";
            this.root.css({
                'background': 'transparent',
                'pointer-events': 'none'
            });
        } else if (this.type == "error") {
            typeClass = "alert-danger";
            typeTitle = "错误";
        }
        var html = "<div class='alert " + typeClass + " alert-dismissible' role='alert' style='z-index:999999;position: absolute;left: 10%;bottom: 5%;width: 80%; pointer-events: auto;'>";
        html += "<button type='button' style='margin-right:35px;' class='close messageClose' data-dismiss='alert' aria-label='Close'><span aria-hidden='true' style='color: black;'>&times;</span></button>";
        html += " <div style='word-break: break-all; font-size:15px; line-height:20px;'><strong>" + typeTitle + "!</strong> " + this.content + "</div></div>";
        this.root.append(html);
        $("body").append(this.root);
        this.initEvent();
    };

    MessageAlert.prototype.initEvent = function() {
        var _this = this;
        $(".messageClose", this.root).unbind("click").bind("click", function() {
            _this.root.remove();
        });
        if (this.type != "error") {
            window.setTimeout(function() {
                _this.root.remove();
                _this.toastBusy();
            }, this.timer);
        }
    };

    var toastQueue = [];
    var isToastBusy = false;
    $.MessageAlert = function(opt) {
        if (!opt.content) {
            return;
        }
        if (opt.content.length > 20) {
            opt.content = opt.content.substring(0, 20) + "...";
        }
        if ($config.debug) {
            if (opt.type == 'error') {
                toastQueue = [];
                isToastBusy = false;
                new MessageAlert(opt);
                return;
            }
            if (opt.timer == null || typeof opt.timer == "undefined") {
                opt.timer = 2000;
            }
            if (isToastBusy) {
                toastQueue.push(opt);
            } else {
                isToastBusy = true;
                new MessageAlert(opt);
            }
        } else {
            alert(opt.content);
        }
    };

    MessageAlert.prototype.toastBusy = function() {
        if (isToastBusy) {
            if (toastQueue.length > 0) {
                var opt = toastQueue.shift();
                new MessageAlert(opt);
            } else {
                isToastBusy = false;
            }
        }

        return isToastBusy;
    }
});
