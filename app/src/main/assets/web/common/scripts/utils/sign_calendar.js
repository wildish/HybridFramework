/**
 * $.SignCalendar()是日历签到框
 * 参数：
 * 	signList: 当月签到日期数组集合
 * 使用：
 *  $.SignCalendar([{signDay: "01"},{signDay: "03"},{signDay: "06"},{signDay: "30"}]);
 */
define(['jquery'], function($) {
    function SignCalendar(signList) {
        this.root = $("<div id=\"divcalendar\" class=\"bg_popup\"></div>");
        this.root.append("<div class='loader-waiting'></div>");
        this.$base = null;
        // 当前日历显示的年份
        this.showYear = 2016;
        // 当前日历显示的月份
        this.showMonth = 1;
        // 当前日历显示的天数
        this.showDays = 1;
        this.eventName = "load";
        this.init(signList);
    }

    SignCalendar.prototype.init = function(signList) {
        var _this = this;
        _this.setMonthAndDay();
        _this.draw(signList);
        _this.bindEnvent();
    };

    SignCalendar.prototype.draw = function(signList) {
        var _this = this;
        // 绑定日历
        var html = _this.drawCal(_this.showYear, _this.showMonth, signList);
        this.root.append(html);
        $("body").append(this.root);
        // 绑定日历表头
        var calendarName = _this.showYear + "年" + _this.showMonth + "月";
        $(".calendar_month_span").html(calendarName);
    };

    SignCalendar.prototype.bindEnvent = function() {
        var _this = this;
        $("#divcalendar").click(function(event) {
            _this.cancel();
            return false;
        });
        // 绑定上个月事件
        $(".calendar_month_prev").click(function() {
            // ajax获取日历json数据
            var signList = [{
                "signDay": "10"
            }, {
                "signDay": "11"
            }, {
                "signDay": "12"
            }, {
                "signDay": "13"
            }];
            _this.eventName = "prev";
            _this.init(signList);
        });
        // 绑定下个月事件
        $(".calendar_month_next").click(function() {
            // ajax获取日历json数据
            var signList = [{
                "signDay": "10"
            }, {
                "signDay": "11"
            }, {
                "signDay": "12"
            }, {
                "signDay": "13"
            }];
            _this.eventName = "next";
            _this.init(signList);
        });
    };
    SignCalendar.prototype.setMonthAndDay = function() {
        var _this = this;
        switch (_this.eventName) {
            case "load":
                var current = new Date();
                _this.showYear = current.getFullYear();
                _this.showMonth = current.getMonth() + 1;
                break;
            case "prev":
                var nowMonth = $(".calendar_month_span").html().split("年")[1].split("月")[0];
                _this.showMonth = parseInt(nowMonth) - 1;
                if (_this.showMonth == 0) {
                    _this.showMonth = 12;
                    _this.showYear -= 1;
                }
                break;
            case "next":
                var nowMonth = $(".calendar_month_span").html().split("年")[1].split("月")[0];
                _this.showMonth = parseInt(nowMonth) + 1;
                if (_this.showMonth == 13) {
                    _this.showMonth = 1;
                    _this.showYear += 1;
                }
                break;
        }
    };
    SignCalendar.prototype.getDaysInmonth = function(iMonth, iYear) {
        var _this = this;
        var dPrevDate = new Date(iYear, iMonth, 0);
        return dPrevDate.getDate();
    };

    SignCalendar.prototype.getBuildRow = function(iYear, iMonth) {
        var _this = this;
        var dCalDate = new Date(iYear, iMonth - 1, 1);
        var iDayOfFirst = dCalDate.getDay();
        var dDate = new Date(iYear, iMonth, 0);
        var daycount = dDate.getDate();
        var tr_nums = Math.ceil((iDayOfFirst + daycount) / 7);
        return tr_nums;
    };

    SignCalendar.prototype.bulidCal = function(iYear, iMonth) {
        var _this = this;
        var aMonth = new Array();
        aMonth[0] = new Array(7);
        aMonth[1] = new Array(7);
        aMonth[2] = new Array(7);
        aMonth[3] = new Array(7);
        aMonth[4] = new Array(7);
        aMonth[5] = new Array(7);
        aMonth[6] = new Array(7);
        var dCalDate = new Date(iYear, iMonth - 1, 1);
        var iDayOfFirst = dCalDate.getDay();
        var iDaysInMonth = _this.getDaysInmonth(iMonth, iYear);
        var iVarDate = 1;
        var d, w;
        aMonth[0][0] = "日";
        aMonth[0][1] = "一";
        aMonth[0][2] = "二";
        aMonth[0][3] = "三";
        aMonth[0][4] = "四";
        aMonth[0][5] = "五";
        aMonth[0][6] = "六";
        for (d = iDayOfFirst; d < 7; d++) {
            aMonth[1][d] = iVarDate;
            iVarDate++;
        }
        for (w = 2; w < 7; w++) {
            for (d = 0; d < 7; d++) {
                if (iVarDate <= iDaysInMonth) {
                    aMonth[w][d] = iVarDate;
                    iVarDate++;
                }
            }
        }
        return aMonth;
    };
    SignCalendar.prototype.ifHasSigned = function(signList, day) {
        var _this = this;
        var signed = false;
        $.each(signList, function(index, item) {
            if (item.signDay == day) {
                signed = true;
                return false;
            }
        });
        return signed;
    };
    SignCalendar.prototype.drawCal = function(iYear, iMonth, signList) {
        var _this = this;
        var myMonth = _this.bulidCal(iYear, iMonth);
        var htmls = new Array();
        htmls.push("<div class='sign_main' id='sign_layer'>");
        htmls.push("<div class='sign_succ_calendar_title'>");
        // htmls.push("<div class='calendar_month_next'>下月</div>");
        // htmls.push("<div class='calendar_month_prev'>上月</div>");
        htmls.push("<div class='calendar_month_span'></div>");
        htmls.push("</div>");
        htmls.push("<div class='sign' id='sign_cal'>");
        htmls.push("<table>");
        htmls.push("<tr>");
        htmls.push("<th>" + myMonth[0][0] + "</th>");
        htmls.push("<th>" + myMonth[0][1] + "</th>");
        htmls.push("<th>" + myMonth[0][2] + "</th>");
        htmls.push("<th>" + myMonth[0][3] + "</th>");
        htmls.push("<th>" + myMonth[0][4] + "</th>");
        htmls.push("<th>" + myMonth[0][5] + "</th>");
        htmls.push("<th>" + myMonth[0][6] + "</th>");
        htmls.push("</tr>");
        var d, w;
        var tr_nums = _this.getBuildRow(iYear, iMonth);
        for (w = 1; w < tr_nums + 1; w++) {
            htmls.push("<tr>");
            for (d = 0; d < 7; d++) {
                var ifHasSigned = _this.ifHasSigned(signList, myMonth[w][d]);
                if (ifHasSigned) {
                    htmls.push("<td class='on'>" + (!isNaN(myMonth[w][d]) ? myMonth[w][d] : " ") + "</td>");
                } else {
                    htmls.push("<td>" + (!isNaN(myMonth[w][d]) ? myMonth[w][d] : " ") + "</td>");
                }
            }
            htmls.push("</tr>");
        }
        htmls.push("</table>");
        htmls.push("</div>");
        htmls.push("</div>");
        return htmls.join('');
    };

    SignCalendar.prototype.cancel = function() {
        this.root.remove();
    };

    $.SignCalendar = function(signList) {
        new SignCalendar(signList);
    };
});
