/* 实现功能
        tab选项卡切换，支持手势滑动，配合tabcontrol.css使用
   使用规则
    参数：
    param 对象
    param.wrap iscrollwrap（外层的div，视域）
    param.types tab选项集合
    param.theme 主题样式
    param.position tabHeader位置（顶部或底部）
    eg :
    var detailTabcontroll = new ITabcontrol({
       wrap : "scenic_detail_tabWrapper",
       types : tabTypes,
       theme : "c",
       position : "top",
       onSelectChange : function(fromIndex, toIndex, lastContent, currentContent) {
          if (fromIndex != toIndex) {
             // proxy last content;
          }
          typeChangedTo(toIndex, currentContent);
       }
    });
*/
define([ "jquery", "touch" ], function($, touch) {
	function ITabcontrol(param) {
		this.params = $.extend({
			wrap : null,
			types : [],
			theme : "a",
			position : "top",
			onSelectChange : null,
			onLoaded : null
		}, param);

		this.selectIndex = 0;
		this.$container;
		this.$header;
		this.$content;

		this._initData();
		this._initUI();
	}
	;

	/**
	 * 初始化参数和数据
	 */
	ITabcontrol.prototype._initData = function() {
		if (!this.params.wrap) {
			console.error("wrap参数未指定");
			return;
		}
	};
	ITabcontrol.prototype._initUI = function() {
		var _this = this;
		_this.$container = $("#" + _this.params.wrap);
		_this.$header = $("<div class='tabcontrol_header'></div>");
		_this.$header.removeClass(_this.params.theme).addClass(_this.params.theme);

		_this.$content = $("<div class='tabcontrol_maincontent'></div>");

		var itemWidth = (_this.$container.width() - this.params.types.length + 1) / _this.params.types.length;
		_this.params.types.forEach(function(type) {
			var item = $("<div class='tabcontrol_item'><span style='height:50%;'>" + type + "</span></div>");
			switch (_this.params.theme) {
			case "a":
				item.css("width", itemWidth);
				break;
			case "b":
				item.css("width", itemWidth);
				break;
			case "c":
				item.css("width", itemWidth);
				break;
			default:
				item.css("width", itemWidth);
				break;

			}

			var contentItem = $("<div class='tabcontrol_subcontent tabcontrol_contenthidden'></div>");
			// contentItem.html("This is the " + type +" content.")

			var index = _this.params.types.indexOf(type);
			if (index == _this.selectIndex) {
				item.removeClass("tabcontrol_itemselected").addClass("tabcontrol_itemselected");
				contentItem.removeClass("tabcontrol_contenthidden");
			}

			item.click(function(e) {
				if (index != _this.selectIndex) {
					var lastSelectIndex = _this.selectIndex;
					var lastSelectItem = _this._getHeaderItemByIndex(_this.selectIndex);
					lastSelectItem.removeClass("tabcontrol_itemselected");
					var lastContentItem = _this._getContentItemByIndex(_this.selectIndex);
					lastContentItem.removeClass("tabcontrol_contenthidden").addClass("tabcontrol_contenthidden");

					_this.selectIndex = index;
					$(this).removeClass("tabcontrol_itemselected").addClass("tabcontrol_itemselected");
					var currentContentItem = _this._getContentItemByIndex(_this.selectIndex);
					currentContentItem.removeClass("tabcontrol_contenthidden");
					if (_this.params.onSelectChange && typeof _this.params.onSelectChange == "function") {
						_this.params.onSelectChange.apply(this, [ lastSelectIndex, _this.selectIndex, lastContentItem, currentContentItem ]);
					}
				}
			});
			_this.$header.append(item);
			_this.$content.append(contentItem);
		});

		touch.on('#target', 'touchstart', function(ev) {
			ev.preventDefault();
		});

		var target = _this.$content[0];
		touch.on(target, 'swipeleft', function(ev) {
            if(ev.target.className.indexOf("scenicimage")>= 0){
              return;
            }
			var index;
			if (_this.selectIndex == _this.params.types.length - 1) {
				index = 0;
			} else {
				index = _this.selectIndex + 1;
			}
			var expr = ":eq(" + index + ")";
			_this.$header.children(expr).click();
		});

		touch.on(target, 'swiperight', function(ev) {
            if(ev.target.className.indexOf("scenicimage")>= 0){
               return;
            }
			var index;
			if (_this.selectIndex == 0) {
				index = _this.params.types.length - 1;
			} else {
				index = _this.selectIndex - 1;
			}
			var expr = ":eq(" + index + ")";
			_this.$header.children(expr).click();

		});

		if (_this.params.position == "top") {
			_this.$container.append(_this.$header);
			_this.$container.append(_this.$content);
		} else {
			_this.$container.append(_this.$content);
			_this.$container.append(_this.$header);
		}

		setTimeout(function() {
			_this.$content.height(_this.$container.height() - _this.$header.height());
		}, 200);
		if (_this.params.onSelectChange && typeof _this.params.onSelectChange == "function") {
			var contentItem = _this._getContentItemByIndex(_this.selectIndex);
			_this.params.onSelectChange.apply(this, [ _this.selectIndex, _this.selectIndex, contentItem, contentItem ]);
		}
	};

	ITabcontrol.prototype._getHeaderItemByIndex = function(index) {
		var expr = ":eq(" + index + ")";
		var headerItem = this.$header.children(expr);
		return headerItem;
	}

	ITabcontrol.prototype._getContentItemByIndex = function(index) {
		var expr = ":eq(" + index + ")";
		var contentItem = this.$content.children(expr);
		return contentItem;
	}
	ITabcontrol.prototype.changeType = function(typeIndex){
		var _this = this;
	    if(typeIndex >=0 && typeIndex < this.params.types.length){
	        var expr = ":eq(" + typeIndex + ")";
	        _this.$header.children(expr).click();
	    }
	}
	return ITabcontrol;
});