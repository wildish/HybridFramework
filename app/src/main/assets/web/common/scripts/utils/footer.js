/*global define:true*/
define(['jquery'], function($) {
    function Footer(id) {
        this.id = id;
        this.$base = null;
        this._visible = true;
        this._currStyle = "footer-common";

        this._init();
    }

    Footer.prototype._init = function() {
        this.$base = $("#" + this.id);
        this.$base.addClass("footer").addClass(this._currStyle);
    };

    Footer.prototype.hide = function() {
        this._visible = false;
        this.$base.removeClass("footer");
        this.$base.hide();
    };

    Footer.prototype.getHeight = function() {
        if (this._visible) {
            return this.$base.height();
        } else {
            return 0;
        }
    };

    Footer.prototype.show = function() {
        this._visible = true;
        this.$base.addClass("footer");
        this.$base.show();
    };

    Footer.prototype.isVisible = function() {
        return this._visible;
    };

    Footer.prototype.setText = function(text) {
        this.$base.html(text);
    };

    Footer.prototype.setOnClick = function(callback) {
        this.$base.unbind("click").bind("click", function() {
            callback.call(this);
        });
    };

    Footer.prototype.reset = function() {
        this.$base.removeClass(this._currStyle).addClass("footer-common");
        this._currStyle = "footer-common";
        this.$base.unbind("click");
        this.$base.html("");
        this.$base.hide();
    };

    Footer.prototype.getHeight = function() {
        return this.$base.height();
    };

    Footer.prototype.setBackground = function(cssStyle) {
        if (typeof cssStyle == "undefined" || cssStyle == null || cssStyle.length == 0) {
            return;
        }

        this.$base.removeClass(this._currStyle).addClass(cssStyle);
        this._currStyle = cssStyle;
    };

    return Footer;
});
