/**
 * $.MessageBox()是对话框
 * 参数：
 * 	title: 对话框的标题
 * 	content： 对话框中的内容
 * 	callback： 回调函数
 * 使用：
 *  $.MessageBox({title: "删除数据", content: "确定删除数据吗？", callback: function(){alert("删除成功");}, onCancel: function(){}});
 */
define(['jquery'], function($) {
	function MessageBox(opt) {
        this.root = $("<div id=\"messagebox\" class=\"bg_popup\"></div>");
        this.root.append("<div class='loader-waiting'></div>");
		opt = opt || {};
		this.title = opt.title || "提示";
		this.content = opt.content || "内容";
		this.callback = opt.callback || function() {};
		this.onCancel = opt.onCancel || function() {};
		this.initHtml();
	};
	
	MessageBox.prototype.initHtml = function() {
		var html = "<div class='messagebox-wrap'>"
	        + "         <div class='messagebox-title'>" + this.title + "</div>"
		    + "         <div class='messagebox-content'>" + this.content + "</div>"
			+ "         <div class='messagebox-btn'>"
			+ "             <button id='messagebox_cancel' class='btn'>取消</button>"
			+ "             <button id='messagebox_ok' class='btn btn-primary' style='background-color: #e26d5c;margin-left:10px;'>确定</button>"
			+ "         </div>"
			+ "     </div>";

		this.root.append(html);
		$("body").append(this.root);
		this.initEvent();
	};
	
	MessageBox.prototype.initEvent = function() {
		var _this = this;
		$("#messagebox_ok").unbind("click").bind("click", function() {
			_this.cancel();
			_this.callback();
		});
		
		$("#messagebox_cancel").unbind("click").bind("click", function() {
			_this.cancel();
            _this.onCancel();
		});
	};

	MessageBox.prototype.cancel = function() {
	    this.root.remove();
	};

 	$.MessageBox = function(opt) {
 		new MessageBox(opt);
 	};
});