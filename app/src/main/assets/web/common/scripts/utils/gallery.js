﻿ /*
 实现功能
    图片幻灯片播放功能，配合gallery.css使用。

使用规则
    params  传入参数对象
        params.warp   外层的div，视域
        params.autoplay  是否自动播放
        params.interval  自动播放间隔时间(毫秒)
        params.onLoadad  加载后回调函数
        eg:
        var myGallery = new IGallery({
                wrap:"div",
               autoPlay : true，
               interval : 2000,
               onLoaded : function(event){
                       alert("haha..");
               }
        });
        */
define(["jquery", "touch"], function ($, touch) {
    function IGallery(param) {
        this.params = $.extend({
            wrap: null,
            hasTips: false,
            autoPlay: false,
            interval: 3000,
            type:"a",
            onLoaded: null
        }, param);

        this.selectIndex = 0;
        this.$container;
        this.$scroll;
        this.$items;
        this.$tips;
        this._iscrollImpl = null;

        this._initData();
        this._initUI();
    };

    /**
	 * 初始化参数和数据
	 */
    IGallery.prototype._initData = function () {
        if (!this.params.wrap) {
            console.error("wrap参数未指定");
            return;
        }
    };
    IGallery.prototype._initUI = function () {
        var _this = this;
        _this.$container = $("#" + _this.params.wrap);
        _this.$scroll = $("<div class='gallery_scroll'></div>");
        _this.$items = $("<div class='gallery_items'></div>");

        _this.$scroll.append(_this.$items);
        _this.$scroll.width(_this.$container.width());
        _this.$scroll.height(_this.$container.height());

        _this.$container.append(_this.$scroll);
        if (_this.params.hasTips) {
            _this.$tips = $("<div class='gallery_tips'></div>");
            _this.$tips.width(_this.$container.width());
            _this.$container.append(_this.$tips);
        }
        if (_this.params.onLoaded) {
            _this.params.onLoaded.call(this);
        }
    };

    IGallery.prototype.additems = function (items) {
        var _this = this;

        $(items).each(function (index, item) {
            _this.$items.append(item);
            _this.$items.width(item.width() * (index + 1));
            _this.$items.height(item.height());
            if (_this.params.hasTips) {
                var tipStr = "<div class='gallery_tip'><div class='point "+_this.params.type +" '></div></div>";
                var $tip = $(tipStr);
                _this.$tips.append($tip);
            }
        });
        _this._galleryImpl();
    };

    IGallery.prototype._galleryImpl = function () {
        // tips
        var childNodes = this.$scroll.children().children();
        if (this.params.hasTips && this.$tips.length > 0) {
            var left = 0;
            var dot = this.$tips.children().first();
            var dw = dot.width();
            var childCount = childNodes.length - 1;
            dot.addClass("selected");
            for (var i = 0; i <= childCount; i++) {
                dot = this.$tips.children().eq(i);
                dot.css("left", left + "px");
                left += dw;
            }
            this.$tips.width(dw * (childCount + 1));
            this.$tips.css("right", (this.$container.width()- this.$tips.width())/2.0 +"px");
        }

        this._scrollImpl();
        if (this.params.autoPlay) {
            this.start();
        }
    };

    IGallery.prototype._scrollImpl = function () {
        var _this = this;
        var opt = {};
        opt.useTransition = true;
        opt.hScrollbar = false;
        opt.vScrollbar = false;
        opt.hideScrollbar = true;
        opt.bounce = false;
        opt.bounceLock = true;
        opt.momentum = true;
        opt.lockDirection = true;
        opt.checkDOMChanges = false;
        opt.topOffset = 0;
        opt.vScroll = false;
        opt.hScroll = true;
        opt.onRefresh = function () {
            this.enable();
        };
        opt.onScrollMove = function () {

        };

        opt.onScrollEnd = function () {

        };
        opt.onBeforeScrollStart = function () {
            _this.stop();
        };
        opt.onTouchEnd = function (e) {
            var animationTime = _this.params.animationTime || 1000;
            if (_this._iscrollImpl.dirX == -1) {
                if (_this._iscrollImpl.absDistX >= _this.$container.width() / 3.0) {
                    _this._onScroll(false);
                } else {
                    var x = (- _this.selectIndex) * _this.$container.width();
                    _this._iscrollImpl.scrollTo(x, 0, animationTime, false);
                }
            } else if (_this._iscrollImpl.dirX == 1) {
                if (_this._iscrollImpl.absDistX >= _this.$container.width() / 3.0) {
                    _this._onScroll(true);
                } else {
                    var x = (- _this.selectIndex) * _this.$container.width();
                    _this._iscrollImpl.scrollTo(x, 0, animationTime, false);
                }
            } else {
                var x = (-_this.selectIndex) * _this.$container.width();
                _this._iscrollImpl.scrollTo(x, 0, animationTime, false);
            }

            setTimeout(function () {
                _this._iscrollImpl.dirX = 0;
                _this._iscrollImpl.dirY = 0;
                _this._iscrollImpl.absDistX = 0;
                _this._iscrollImpl.absDistY = 0;
            },100);

            if (_this.params.autoPlay) {
                //_this.start();
            }
        };

        //
        this.selectIndex = 0;
        this._iscrollImpl = $win.touchScroll(this.$scroll, opt);
        this._iscrollImpl.disable();
        this._iscrollImpl.refresh();
    };

    IGallery.prototype.start = function () {
        //
        var _this = this;
        //this.selectIndex = 0;
        var interval = this.params.interval || 3000;
        var animationTime = this.params.animationTime || 1000;
        //this._iscrollImpl.scrollTo(-this.$container.width(), 0, 0, false);
        //this._iscrollImpl.refresh();
        this._timer = setInterval(function () {
            _this._onScroll(true);
        }, parseInt(interval) + parseInt(animationTime));
    };

    IGallery.prototype.stop = function () {
        if (this._timer != null) {
            clearInterval(this._timer);
            this._timer = null;
        }
    };

    IGallery.prototype._onScroll = function (ispageadd) {
        //
        var childNodes = this.$scroll.children().children();
        if (childNodes.length < 1) {
            return;
        }
        //
        this.$tips.children().eq(this.selectIndex).removeClass("selected");
        if (ispageadd) {
            this.selectIndex++;
        } else {
            this.selectIndex--;
        }
        var count = childNodes.length - 1;
        var animationTime = this.params.animationTime || 1000;
        if (this.selectIndex > count) {
            this._iscrollImpl.scrollTo(0, 0, 0, false);
            //this._iscrollImpl.refresh();
            this.selectIndex = 0;
        } else if (this.selectIndex < 0) {
            this._iscrollImpl.scrollTo((-count) * this.$container.width(), 0, 0, false);
            //this._iscrollImpl.refresh();
            this.selectIndex = count;

        } else {
            var x = (-this.selectIndex) * this.$container.width();
            this._iscrollImpl.scrollTo(x, 0, animationTime, false);
        }
        this.$tips.children().eq(this.selectIndex).addClass("selected");
        //
    };

    IGallery.prototype.destroy = function () {
        this.stop();
        if (this._iscrollImpl != null) {
            this._iscrollImpl.destroy();
            this._iscrollImpl = null;
        }
        this.$tips = null;
        this.$items = null;
        this.$scroll = null;
        this.selectIndex = 0;
        //
    };




    IGallery.prototype._getHeaderItemByIndex = function (index) {
        var expr = ":eq(" + index + ")";
        var headerItem = this.$header.children(expr);
        return headerItem;
    }

    IGallery.prototype._getContentItemByIndex = function (index) {
        var expr = ":eq(" + index + ")";
        var contentItem = this.$content.children(expr);
        return contentItem;
    }

    return IGallery;
});