﻿
define(['jquery'], function($) {
    function ShoppingcartItem(params) {
        this.params = $.extend({
            id: "shoppingcart-item",
            image: "shoppingcart.png",
            onClick: function() {

            },
            onRefresh: function() {

            }
        }, params);
        this.$base = null;
        this.$number = null;
        this._init();
    }

    ShoppingcartItem.prototype._init = function() {
        var _this = this;
        this.$base = $("<div id='" + this.params.id + "' class='shoppingcart_icon'></div>");
        this.$base.css({
            'margin': '5px',
            'height': '40px',
            'width': '40px',
            'background-image': 'url(../common/images/icon/' + _this.params.image + ')',
            'background-position': 'center',
            'background-size': '20px 20px'
        });
        this.$number = $("<div class='shoppingcart_bridge'></div>");
        this.$number.css({
            'position': 'relative',
            'left': '20px',
            'top': '5px',
            'width': '15px',
            'height': '15px',
            'font-size': '10px',
            'color': '#FFF',
            'background-color': '#F00',
            'text-align': 'center',
            'border-radius': '7.5px',
            'line-height': '15px'
        });
        this.$number.css('visibility', 'hidden');
        this.$base.append(this.$number);
        this.$base.unbind("touchstart");
        this.$base[0].ontouchstart = function() {
            $(this).css({
                opacity: 0.5
            });
        };
        this.$base.unbind("touchend");
        this.$base[0].ontouchend = function() {
            $(this).css({
                opacity: 1.0
            });
        };
        this.$base.unbind("click");
        this.$base.click(function() {
            if (_this.params.onClick && typeof _this.params.onClick == "function") {
                _this.params.onClick.apply(this);
            }
        });
    };
    ShoppingcartItem.prototype.number = function(n) {
        if (n == 0) {
            this.$number.css('visibility', 'hidden');
        } else {
            this.$number.css('visibility', 'visible');
            this.$number.html(n);
        }
    };
    ShoppingcartItem.prototype.refresh = function(count) {
        if (this.params.onRefresh && typeof this.params.onRefresh == "function") {
            this.params.onRefresh.apply(this);
        }
    };

    return ShoppingcartItem;
});
