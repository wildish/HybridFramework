define(['jquery'], function($) {
	var kk = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-=";
	$.b = function(s, k) {
		var r = [];
		var idx = 0, c1, c2, c3, kc;
		while (idx + 2 < s.length) {
			c1 = 0x00FF & s.charCodeAt(idx++);
			c2 = 0x00FF & s.charCodeAt(idx++);
			c3 = 0x00FF & s.charCodeAt(idx++);
			kc = (c1 << 16) + (c2 << 8) + c3 + k;
			r.push(kk[kc & 0x3F]);
			r.push(kk[(kc >> 6) & 0x3F]);
			r.push(kk[(kc >> 12) & 0x3F]);
			r.push(kk[(kc >> 18) & 0x3F]);
		}
		if (idx + 1 == s.length) {
			kc = (s.charCodeAt(idx) << 16) + k;
			r.push(kk[kc & 0x3F]);
			r.push(kk[(kc >> 6) & 0x3F]);
			r.push(kk[(kc >> 12) & 0x3F]);
			r.push(kk[(kc >> 18) & 0x3F]);
		} else if (idx + 2 == s.length) {
			kc = (s.charCodeAt(idx) << 16)
					+ (s.charCodeAt(idx + 1) << 8) + k;
			r.push(kk[kc & 0x3F]);
			r.push(kk[(kc >> 6) & 0x3F]);
			r.push(kk[(kc >> 12) & 0x3F]);
			r.push(kk[(kc >> 18) & 0x3F]);
		}
		return r.join("");
	};
	
	$.ab = function(pa, pb) {
		var va = "", vb = "", vk = window.k || 0, vp = pa.indexOf('?');
		if (vp > 0) {
			va = pa.substring(0, vp), vb = pa.substring(vp);
			if (pb)
				vb += "&" + pb;
		} else {
			va = pa, vb = "?" + pb;
		}
		//
		var ar = vb.match('[?&]action=([^&]*)');
		if (ar) {
			va += "?action=" + ar[1];
			if (ar[1] == "doLogin") {
				return {
					a : va,
					b : $.b(vb, 0)
				};
			} else {
				return {
					a : va,
					b : $.b(vb, vk)
				};
			}
		} else {
			va += "?action=doLogin";
			return {
				a : va,
				b : $.b(vb, 0)
			};
		}
	};
	
	$.json2str = function(params) {
		var kv = "";
		if (typeof (params) != "string") {
			for ( var i in params) {
				kv += encodeURIComponent(i) + "="
						+ encodeURIComponent(params[i]) + "&";
			}
		} else {
			kv = params;
		}
		return kv;
	};
	
	$.post = function(url, data, success, dataType) {
		if(Waiting && Waiting.show) {
			Waiting.show();
		}
		var ab = $.ab(url, $.json2str(data));
		var options = {
			url : ab.a,
			type : "POST",
			processdata : false,
			contentType : "text/plain",
			data : ab.b,
			success : function(data) {
				if(Waiting && Waiting.hide) {
					Waiting.hide();
				}
				success(data);
			},
			dataType : dataType,
			timeout : 10000
		};
		return $.ajax(options);
	};
});