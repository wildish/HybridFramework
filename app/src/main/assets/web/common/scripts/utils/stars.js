/*实现功能
   星级评分

使用规则
   params  传入参数对象
       params.warp          外层的div的id，视域
       params.count         星星数量，默认5
       params.onSelect      选择后回调函数
       params.color         填充颜色
       params.borderColor   外边界颜色
       eg:
       var myStars = new IStars({
               wrap:"div",
               count : true,
               color: "#827839",
               borderColor: "#827839",
               onSelect : function(i){
                      alert(i+1 +"星");
              }
       });
*/
/*global define:true*/
define(["jquery"], function($) {
    function IStars(param) {
        this.params = $.extend({
            wrap: null,
            count: 5,
            color: "#827839",
            borderColor: "#827839",
            onSelect: function(i) {
                console.log(i + 1 + "星");
            }
        }, param);

        this.selectIndex = 0;
        this.$container;
        this._initUI();
        this._initEvents();
    }

    /**
     * 首先根据星星数目平分容器宽度，之后在各自区域绘制星星(水平方向左右5%留白，剩余区域以宽高最小值计算星星半径后居中画星)
     */
    IStars.prototype._initUI = function() {
        var _this = this;
        this.container = $("#" + this.params.wrap);
        var count = this.params.count;
        var itemWidth = this.container.width() / count;
        var itemHeight = this.container.height();
        for (var i = 0; i < count; i++) {
            var itemHtml = "<canvas class=\"draw-star-canvas\" width=\"" + itemWidth + "px\" height=\"" + itemHeight + "px\"></canvas>";
            this.container.append(itemHtml);
        }
        this.container.children("canvas").each(function(index, e) {
            _this._drawStar(e, false);
        });
    };
/**
 * 点击星星事件，重绘所有星星，并触发星星点击回调事件，传递被点击星星index参数
 */
    IStars.prototype._initEvents = function() {
        var _this = this;
        this.container.children("canvas").each(function(index, e) {
            $(e).unbind("click").click(function() {
                var i = _this.container.children("canvas").index(this);
                _this._drawStar(this, true);
                var expr1 = "canvas:gt(" + i + ")";
                _this.container.children(expr1).each(function() {
                    _this._drawStar(this, false);
                });
                var expr2 = "canvas:lt(" + i + ")";
                _this.container.children(expr2).each(function() {
                    _this._drawStar(this, true);
                });
                if (_this.params.onSelect && typeof _this.params.onSelect == "function") {
                    _this.params.onSelect.apply(_this, [i]);
                }
            });
        });
    };
/**
 * [_drawStar description]
 * @param  {[type]}  dom      星星画布
 * @param  {Boolean} isSelect 星星是否选择
 */
    IStars.prototype._drawStar = function(dom, isSelect) {
        var ctx = dom.getContext('2d');
        if (isSelect) {
            ctx.fillStyle = this.params.color;
        } else {
            ctx.fillStyle = "transparent";
        }
        var w = dom.clientWidth;
        var h = dom.clientHeight;
        w = $(dom).width() * 0.9;
        h = $(dom).height();
        ctx.clearRect(0, 0, w, h);
        var du = Math.PI / 180;
        var d = Math.min(w, h) * (Math.sin(72 * du) / (1 + Math.sin(54 * du)));
        if (d > w*0.9 / 2.0) {
            d = w*0.9 / 2.0;
        }
        var diy = (h - (1 + Math.sin(54 * du)) / Math.sin(72 * du) * d) / 2.0;
        var dix = (w - 2 * d) / 2.0 + 0.05 * w;
        ctx.beginPath();
        ctx.moveTo(d + dix, 0 + diy);
        ctx.lineTo((1 - 1 / (Math.tan(72 * du) * Math.tan(54 * du))) * d + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo(0 + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo((1 - Math.sin(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + dix, (1 / Math.sin(72 * du) + Math.cos(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + diy);
        ctx.lineTo(((1 - Math.cos(54 * du)) / Math.sin(72 * du)) * d + dix, ((1 + Math.sin(54 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo(d + dix, (1 / Math.sin(72 * du) + 1 / (Math.tan(72 * du) * Math.sin(54 * du))) * d + diy);
        ctx.lineTo(((1 + Math.cos(54 * du)) / Math.sin(72 * du)) * d + dix, ((1 + Math.sin(54 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo((1 + Math.sin(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + dix, (1 / Math.sin(72 * du) + Math.cos(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + diy);
        ctx.lineTo(2 * d + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo((1 + 1 / (Math.tan(72 * du) * Math.tan(54 * du))) * d + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.closePath();
        ctx.fill();
        // if (!isSelect) {
        ctx.strokeStyle = this.params.borderColor;
        ctx.stroke();
        // }
    };
    return IStars;
});
