define([], function() {
    function HybridHelper() {
        // callback map
        this._cbMap_ = {};

    }
    HybridHelper.METHOD_KEY = "method_key";

    HybridHelper.prototype.proxyExecute = function(scheme, proxy, method, params, callback) {
        // 规则检查
        if(!$.isNotBlank(proxy) || !$.isNotBlank(method)) {
            return;
        }

        params[HybridHelper.METHOD_KEY] = method;
        if(callback && typeof callback == "function") {
            this.proxyRegister(proxy + "_" + method, callback);
        }
        var url = scheme + "://" + proxy + "?" + Base64.encode(JSON.stringify(params));
        window.location.href = url;
    };

    HybridHelper.prototype.proxyRegister = function(key, callback) {
        if (this._cbMap_.hasOwnProperty(key)) {
            console.log("删除事件" + key);
            delete this._cbMap_[key];
        }
        if (typeof callback != "undefined" && callback != null) {
            console.log("注册事件" + key);
            this._cbMap_[key] = callback;
        }
    };

    HybridHelper.prototype.proxyCallback = function(proxy, method, params) {
        var key = proxy + "_" + method;
        if (this._cbMap_.hasOwnProperty(key)) {
            var cb = this._cbMap_[key];
            cb.call(this, params);
            this.proxyRegister(key);
        } else {
            console.log("事件" + key + "未注册。");
        }
    };

    return HybridHelper;
});