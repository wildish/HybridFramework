/*global define define:true*/
define(["jquery"], function($) {
    function PageMsgBox(params) {
        this.params = params;
    }

    PageMsgBox.prototype.initDom = function() {

    };

    PageMsgBox.prototype.initData = function() {

    };

    PageMsgBox.prototype.initEvent = function() {
        $("#msgbox").unbind("click").bind("click", function() {
            $.MessageBox({
                title: "删除数据",
                content: "确定删除数据吗?",
                callback: function() {
                    $.MessageAlert({
                        content: "删除成功"
                    });
                },
                onCancel: function() {
                    $.MessageAlert({
                        content: "取消删除"
                    });
                }
            });
        });
    };

    return PageMsgBox;
});
