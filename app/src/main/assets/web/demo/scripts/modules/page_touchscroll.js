/*global define $win:true*/
define(["jquery"], function($) {
    function PageTouchScroll() {

        this.scroller = null;
    }

    PageTouchScroll.prototype.initDom = function() {
        $("#wrapper").height($win.height - $win.getTopbar().getHeight() - 100);
        $win.topbar.setTitle("页面滚动");
    };

    PageTouchScroll.prototype.initData = function() {
        var html = this.loadData(10);
        $(".scroller").html(html);
    };

    PageTouchScroll.prototype.initEvent = function() {
        // 使用iScroll4
        // $win.touchScroll("wrapper", {
        //     useTransition: true,
        //     topOffset: 0,
        //     handleClick: true,
        //     vScrollbar: false,
        //     onRefresh: function() {
        //         this.enable();
        //     },
        //     onScrollMove: function() {},
        //     onScrollEnd: function() {
        //         this.handleClick = true;
        //     }
        // });

        var _this = this;
        $("#loadData").unbind("click").bind("click", function() {
            var html = _this.loadData(10);
            $(".scroller").append(html);

            _this.scroller.refresh();
        });


        // 推荐使用iScroll5，注意touchScroll5的第一个参数，需要传递选择器而不是直接id
        this.scroller = $win.touchScroll5("#wrapper");


    };

    PageTouchScroll.prototype.loadData = function(size) {
        var html = "";
        for (var i = 0; i != size; i++) {
            html += "<div class='item'>第" + (i + 1) + "条数据</div>";
        }
        return html;
    };

    return PageTouchScroll;
});
