/*global define $win Topbar:true*/
define(["jquery", "itablecontrol"], function($, ITabControl) {
    function PageTableControlTest(params) {
        this.params = params;
        this.content = "下面是tableControl的内容";
        this.msg = "测试成功";

        this.dataErrorArray = new Array();
    }

    PageTableControlTest.prototype.initDom = function() {
        $("#tableControlTestTitle").children("h2").text(this.content);
        var headerHtml = "<div class='col-xs-4' tabId='tableControlTestPage_a' tabPage='page/demo/tableControlTestPageA' tabModule='page_tablecontrol_test_a' cacheMode='uncached'>Page a</div>" +
            "<div class='col-xs-4' tabId='tableControlTestPage_b' tabPage='page/demo/tableControlTestPageB' tabModule='page_tablecontrol_test_b' cacheMode='uncached'>Page b</div>" +
            "<div class='col-xs-4' tabId='tableControlTestPage_c' tabPage='page/demo/tableControlTestPageC' tabModule='page_tablecontrol_test_c' cacheMode='uncached'>Page c</div>";
        $("#tableControlTestHeader").addClass("row");
        $("#tableControlTestHeader").append(headerHtml);
        $("#tableControlTestContent").height($win.getScreenSize[1] - 50 - $("#tableControlTestTitle").height() - $("#tableControlTestHeader").height());
    };

    PageTableControlTest.prototype.initData = function() {
        var topBarTxt = "测试TableControl";
        $win.topbar.setTitle(topBarTxt);
        $win.topbar.removeItem(Topbar.KEY_MENU);
        $win.topbar.removeItem(Topbar.KEY_BACKHOME);
        $win.topbar.removeItem(Topbar.KEY_BACKWARD);
        $win.topbar.addBackward();
        var html = $("#tableControlTestHeader").html();
        $win.log(html + "****");
        new ITabControl({
            navId: "tableControlTestHeader",
            contentId: "tableControlTestContent",
            defaultTabId: "tableControlTestPage_b",
            loadAll: false,
            selectClass: "selectClass",
            normalClass: "normalClass",
            onSelectChanged: function(lastSelectTabId, currentSelectTabId) {
                $win.log(lastSelectTabId);
                $win.log(currentSelectTabId);
            },
            onSelectLoadBefore: function(selectTabId) {
                $win.log(selectTabId);
                this.loadParams = {
                    "tableControlTestPage_a": {
                        "type": "a"
                    },
                    "tableControlTestPage_b": {
                        "type": "b"
                    },
                    "tableControlTestPage_c": {
                        "type": "c"
                    }
                };
            },
            onSelectLoaded: function(selectTabId) {
                $win.log(selectTabId + "loaded");
                //                                         var params = this.loadParams[selectTabId];
                //                                         require(['page_tablecontrol_test_page'],function(PageTableControlTestPage){
                //                                            var testPage = new PageTableControlTestPage(params);
                //                                            //to do;
                //                                            //页面加载如 initDom等处理
                //                                         })
            }
        });
        //                $win.showContent();

    };

    PageTableControlTest.prototype.initEvent = function() {
        $.MessageAlert({
            "content": this.msg
        });
    };

    PageTableControlTest.prototype.beforeShowContent = function() {
        var flag = false;
        for (var i = 0; i != this.dataErrorArray.length; i++) {
            var value = this.dataErrorArray[i];
            if (value && typeof value == "boolean") {
                flag = true;
                break;
            }
        }
        flag ? $win.showErrorPage() : $win.showContent();
    };

    return PageTableControlTest;
});
