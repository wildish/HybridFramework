/*global define $win:true*/
define(["jquery"], function($) {
    function PageDemo(params) {
        this.params = params;
    }

    PageDemo.prototype.initDom = function() {
        $win.topbar.setTitle("演示");
        $("#wrapper").height($win.height - $win.topbar.getHeight() - 30);
    };

    PageDemo.prototype.initData = function() {

    };

    PageDemo.prototype.initEvent = function() {
        $win.touchScroll("wrapper", {
            useTransition: true,
            topOffset: 0,
            handleClick: true,
            vScrollbar: false,
            onRefresh: function() {
                this.enable();
            },
            onScrollMove: function() {},
            onScrollEnd: function() {
                this.handleClick = true;
            }
        });

        $("ol li a").unbind("click").bind("click",function(){
            var page= $(this).attr("page");
            var modulejs= $(this).attr("modulejs");
            $win.showPage("demo/page/"+page,{moduleName:modulejs},true);
            return false;
        });

        $("#takecall").unbind("click").bind("click", function() {
            $win.getDefaultProxy().takeCall("11111111111");
        });
    };

    return PageDemo;
});
