/*global define $win:true*/
define(['jquery'], function($) {
    function PageNewwinChild(params) {
        this.params = params;
    }

    PageNewwinChild.prototype.initDom = function() {};

    PageNewwinChild.prototype.initData = function() {};

    PageNewwinChild.prototype.initEvent = function() {
        var _this = this;
        $("#goback").unbind("click").bind("click", function() {
            var callbackParams = {
                result: "back to parent parent, " + _this.params.name
            };
            $win.cancel(callbackParams);
        });
        $("#cancel").unbind("click").bind("click", function() {
            var callbackParams = {
                result: "congratulations child, " + _this.params.name
            };
            $win.cancelAll(callbackParams);
        });
    };

    return PageNewwinChild;
});
