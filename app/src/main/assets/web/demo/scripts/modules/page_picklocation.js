/*global define $win:true*/
define(["jquery"], function($) {
    function PagePickLocation() {
    }

    PagePickLocation.prototype.initDom = function() {
        $win.getTopbar().setTitle("地图选点");
    };

    PagePickLocation.prototype.initData = function() {
    };

    PagePickLocation.prototype.initEvent = function() {
        $("#pick").unbind("click").bind("click", function() {
            $win.pickLocation({
                "addr": "郑州市世玺中心"
            }, function(data) {
                if(data.success) {
                    $("#content").append("<h4 class='col-xs-12'>地图直接选点的结果："+JSON.stringify(data)+"</h4>");
                } else {
                    $("#content").append("<h4 class='col-xs-12'>地图直接选点失败</h4>");
                }
            });
        });
        $("#search").unbind("click").bind("click", function() {
            $win.searchLocation({
                "addr": "世玺中心"
            }, function(data) {
                if(data.success) {
                    $("#content").append("<h4 class='col-xs-12'>先搜索再选点的结果："+JSON.stringify(data)+"</h4>");
                } else {
                    $("#content").append("<h4 class='col-xs-12'>先搜索再选点失败</h4>");
                }
            });
        });
        $("#location").unbind("click").bind("click", function() {
            $win.getLocation({
                "addr": "世玺中心"
            }, function(data) {
                if(data.success) {
                    $("#content").append("<h4 class='col-xs-12'>获取定位结果："+JSON.stringify(data)+"</h4>");
                } else {
                    $("#content").append("<h4 class='col-xs-12'>获取定位失败</h4>");
                }
            });
        });
    };

    return PagePickLocation;
});
