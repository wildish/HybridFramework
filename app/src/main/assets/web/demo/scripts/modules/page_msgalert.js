/*global define define:true*/
define(["jquery"], function($) {
    function PageMsgAlert(params) {
        this.params = params;
    }

    PageMsgAlert.prototype.initDom = function() {

    };

    PageMsgAlert.prototype.initData = function() {

    };

    PageMsgAlert.prototype.initEvent = function() {
        $("#msgAlert").unbind("click").bind("click", function() {
            $.MessageAlert({
                content: "我是alert"
            });
        });
    };

    return PageMsgAlert;
});
