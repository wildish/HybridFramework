/*global define $win:true*/
define(["jquery"], function($) {
    function PageTableControlTestPage(params) {
        this.params = params;
        this.content = "下面是tableControl子页面的内容";
        this.msg = "测试成功";
        this.dataErrorArray = new Array();
    }

    PageTableControlTestPage.prototype.initDom = function() {
        $("#tableControlTestPageBContent").children("h2").text(this.content);
    };

    PageTableControlTestPage.prototype.initData = function() {
        var _this = this;
        var xhrArray = new Array();
        var jqXHR = $win.getData({
            "url": {
                "local": "/test/tablecontroltest",
                "remote": ""
            },
            "type": this.params.type
        }, function(result) {
            if (result.success) {
                _this.dataErrorArray.push(false);
                $("#tableControlTestPageBContent").children("h2").append(result.value);
            } else {
                _this.dataErrorArray.push(true);
                $.MessageAlert({
                    "content": "请求数据失败"
                });
            }
        });
        xhrArray.push(jqXHR);
        return xhrArray;
    };

    PageTableControlTestPage.prototype.initEvent = function() {
        $.MessageAlert({
            "content": this.msg
        });
    };

    PageTableControlTestPage.prototype.beforeShowContent = function($tabControl) {
        var flag = false;
        for (var i = 0; i != this.dataErrorArray.length; i++) {
            var value = this.dataErrorArray[i];
            if (value && typeof value == "boolean") {
                flag = true;
                break;
            }
        }
        flag ? $tabControl.showErrorModule() : $tabControl.showContent();
    };

    return PageTableControlTestPage;
});
