/*global define Waiting:true*/
define(["jquery"], function($) {
    function PageWaiting(params) {
        this.params = params;
    }

    PageWaiting.prototype.initDom = function() {

    };

    PageWaiting.prototype.initData = function() {

    };

    PageWaiting.prototype.initEvent = function() {
        $("#waiting").unbind("click").bind("click", function() {
            Waiting.show();

            setTimeout(function(){
                Waiting.hide();
            }, 2000);
        });
    };

    return PageWaiting;
});
