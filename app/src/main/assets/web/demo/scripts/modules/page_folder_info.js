/*global define $win:true*/
define(['jquery'], function($) {
    function PageFolderInfo(params) {
        this.params = params;
    }

    PageFolderInfo.prototype.initDom = function() {
        $win.getDefaultProxy().getTempFolderSize(function(result) {
            $("#result").append("<div class='col-xs-12'><h4>获取临时目录大小的结果:" + JSON.stringify(result) + "</h4></div>");
        });
    };

    PageFolderInfo.prototype.initData = function() {};

    PageFolderInfo.prototype.initEvent = function() {
        $("#clear").unbind("click").bind("click", function() {
            $win.getDefaultProxy().clearTempFolder(function(result) {
                $("#result").append("<div class='col-xs-12'><h4>清理临时目录的结果:" + JSON.stringify(result) + "</h4></div>");
            });
        });
    };

    return PageFolderInfo;
});
