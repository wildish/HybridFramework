/*global define $win:true*/
define(['jquery'], function($) {
    function PageNewwin(params) {
        this.params = params;
    }

    PageNewwin.prototype.initDom = function() {};

    PageNewwin.prototype.initData = function() {};

    PageNewwin.prototype.initEvent = function() {
        $("#showimages").unbind("click").bind("click", function() {

            var images = {
                index: 4,
                images: [
                    {
                        "url": "/sdcard/head.jpg",
                        "mode": 0
                    },
                    {
                        "url": "http://imgsrc.baidu.com/forum/pic/item/66c4dc7fca8065382c4c71f79fdda144af34828d.jpg",
                        "mode": 1
                    },
                    {
                        "url": "http://imgsrc.baidu.com/forum/pic/item/66c4dc7fca8065382c4c71f79fdda144af34828d.jpg",
                        "mode": 1
                    }
                ]
            };

            $win.showImages(images);
        });
    };

    return PageNewwin;
});
