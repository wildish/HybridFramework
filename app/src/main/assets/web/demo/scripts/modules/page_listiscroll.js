/*global define $win window IScroll5List:true*/
define(["jquery"], function($) {
    function PageListIScroll(params) {
        this.params = params;
        this.myScroll;
    }

    PageListIScroll.prototype.initDom = function() {
        $("#wrapper").height($(window).height() - 50 - 50);
        $win.topbar.setTitle("列表控件");
    };

    PageListIScroll.prototype.initData = function() {

    };

    PageListIScroll.prototype.initEvent = function() {
        var _this = this;
        _this.myScroll = new IScroll5List({
            wrap: "wrapper",
            hidePullUp: false,
            hidePullDown: false,
            onLoadData: function(offset) {
                var _this = this;
                loadData(offset, function(itemsize, html) {
                    _this.appendItem(html, itemsize);
                });
            },
            bindEvent: function() {
            }
        });

        function loadData(offset, callback) {
            $win.log("从" + offset + "开始加载10条数据");
            var html = "";
            for (var i = 0; i != 10; i++) {
                html += "<div class='item'>第" + (offset + i + 1) + "条数据</div>";
            }
            callback.call(this, offset + 10, html);
        }

        $("#btnPullup").unbind("click").bind("click", function() {
            var key = JSON.parse($(this).attr("key"));
            if(_this.myScroll) {
                if(key) {
                    _this.myScroll.disablePullUp();
                    $(this).text("启用上拉");
                } else {
                    _this.myScroll.enablePullUp();
                    $(this).text("禁用上拉");
                }
                $(this).attr("key", !key);
            }
        });

        $("#btnPulldown").unbind("click").bind("click", function() {
            var key = JSON.parse($(this).attr("key"));
            if(_this.myScroll) {
                if(key) {
                    _this.myScroll.disablePullDown();
                    $(this).text("启用下拉");
                } else {
                    _this.myScroll.enablePullDown();
                    $(this).text("禁用下拉");
                }
                $(this).attr("key", !key);
            }
        });

        $("#hidePullup").unbind("click").bind("click", function() {
            var key = JSON.parse($(this).attr("key"));
            if(_this.myScroll) {
                if(key) {
                    _this.myScroll.hidePullUp();
                    $(this).text("显示上拉");
                } else {
                    _this.myScroll.showPullUp();
                    $(this).text("隐藏上拉");
                }
                $(this).attr("key", !key);
            }
        });

        $("#hidePulldown").unbind("click").bind("click", function() {
            var key = JSON.parse($(this).attr("key"));
            if(_this.myScroll) {
                if(key) {
                    _this.myScroll.hidePullDown();
                    $(this).text("显示下拉");
                } else {
                    _this.myScroll.showPullDown();
                    $(this).text("隐藏下拉");
                }
                $(this).attr("key", !key);
            }
        });

        $("#showNoData").unbind("click").bind("click", function() {
            if(_this.myScroll) {
                _this.myScroll.showNoData(function() {
                    _this.myScroll.refresh();
                });
            }
        });
    };

    return PageListIScroll;
});
