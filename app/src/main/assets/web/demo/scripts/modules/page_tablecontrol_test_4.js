/*global define $win:true*/
define(["jquery"], function($) {
    function PageTableControlTestPage(params) {
        this.params = params;
        this.content = "下面是tableControl子页面4的内容";
        this.msg = "测试成功";
        this.dataErrorArray = new Array();
    }

    PageTableControlTestPage.prototype.initDom = function() {
        $("#tableControlTestPage4Content").children("h2").text(this.content);
    };

    PageTableControlTestPage.prototype.initData = function() {
        var _this = this;
        var xhrArray = new Array();
        var jqXHR = $win.getData({
            "url": {
                "local": "/test/types",
                "remote": ""
            },
            "type": this.params.type
        }, function(result) {
            if (result.success) {
                _this.dataErrorArray.push(false);
                var data = JSON.parse(result.value);
                for (var i = 0; i < data.length; i++) {
                    var type = data[i];
                    var item = $("<div class='item'></div>");
                    var itemTitle = $("<div class='title col-xs-8'>" + type + "</div>");
                    var itemStatus = $("<div class='selectStatus col-xs-4'></div>");
                    item.append(itemTitle);
                    item.append(itemStatus);
                    $("#tableControlTestPage4Content").append(item);
                }
                $("#tableControlTestPage4Content").children(".item").each(function(index, e) {
                    $(e).unbind("click").click(function() {
                        if ($(this).hasClass("select")) {
                            return;
                        } else {
                            $("#tableControlTestPage4Content").children(".item.select").removeClass("select");
                            $(this).addClass("select");
                        }
                    });
                });
            } else {
                _this.dataErrorArray.push(true);
                $.MessageAlert({
                    "content": "请求数据失败"
                });
            }
        });
        xhrArray.push(jqXHR);
        return xhrArray;
    };

    PageTableControlTestPage.prototype.initEvent = function() {
        $.MessageAlert({
            "content": this.msg
        });
    };

    PageTableControlTestPage.prototype.beforeShowContent = function($tabControl) {
        var flag = false;
        for (var i = 0; i != this.dataErrorArray.length; i++) {
            var value = this.dataErrorArray[i];
            if (value && typeof value == "boolean") {
                flag = true;
                break;
            }
        }
        flag ? $tabControl.showErrorModule() : $tabControl.showContent();
    };

    return PageTableControlTestPage;
});
