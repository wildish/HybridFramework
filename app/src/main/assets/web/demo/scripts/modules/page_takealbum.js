/*global define $win:true*/
define(["jquery"], function($) {
    function PageTakeAlbum(params) {
        this.params = params;
    }

    PageTakeAlbum.prototype.initDom = function() {
        $("#pageContainer").width($win.width);
        $("#pageContainer").height($win.height - $win.getTopbar().getHeight());
        $win.getDefaultProxy().takeAlbum(function(data) {
            $win.log(JSON.stringify(data));
            for (var i = 0; i != data.length; i++) {
                var item = data[i];
                if (item.hasOwnProperty("filepath")) {
                    if (!$.isNotBlank(item.filepath)) {
                        continue;
                    }
                    $("#pageContainer").append("<div style=\"width:30%;height:150px;float:left;margin:5px;\" file-loader='true' image-scale='false' image-type='fitCenter' file-type='pic' file-url='" + item.filepath + "'></div>");
                }
            }
            $win.initDownload($("[file-loader='true']"));
        });
    };

    PageTakeAlbum.prototype.initData = function() {

    };

    PageTakeAlbum.prototype.initEvent = function() {

    };

    return PageTakeAlbum;
});
