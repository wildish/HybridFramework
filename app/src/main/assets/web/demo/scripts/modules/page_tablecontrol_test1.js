/*global define $win Topbar:true*/
define(["jquery", "itablecontrol"], function($, ITabControl) {
    function PageTableControlTest(params) {
        this.params = params;
        this.content = "下面是tableControl的内容";
        this.msg = "测试成功";

        this.dataErrorArray = new Array();
    }

    PageTableControlTest.prototype.initDom = function() {
        $("#tableControlTest1Title").children("h2").text(this.content);
        var headerHtml = "<div class='col-xs-3' tabId='tableControlTestPage_1' tabPage='page/demo/tableControlTestPage1' tabModule='page_tablecontrol_test_1' cacheMode='cached'>Page 1</div>" +
            "<div class='col-xs-3' tabId='tableControlTestPage_2' tabPage='page/demo/tableControlTestPage2' tabModule='page_tablecontrol_test_2' cacheMode='cached'>Page 2</div>" +
            "<div class='col-xs-3' tabId='tableControlTestPage_3' tabPage='page/demo/tableControlTestPage3' tabModule='page_tablecontrol_test_3' cacheMode='cached'>Page 3</div>"+
            "<div class='col-xs-3' tabId='tableControlTestPage_4' tabPage='page/demo/tableControlTestPage4' tabModule='page_tablecontrol_test_4' cacheMode='cached'>Page 4</div>";
        $("#tableControlTest1Header").addClass("row");
        $("#tableControlTest1Header").append(headerHtml);
        $("#tableControlTest1Content").height($win.getScreenSize[1] - 50 - $("#tableControlTest1Title").height() - $("#tableControlTest1Header").height());
    };

    PageTableControlTest.prototype.initData = function() {
        var topBarTxt = "测试TableControl";
        $win.topbar.setTitle(topBarTxt);
        $win.topbar.removeItem(Topbar.KEY_MENU);
        $win.topbar.removeItem(Topbar.KEY_BACKHOME);
        $win.topbar.removeItem(Topbar.KEY_BACKWARD);
        $win.topbar.addBackward();
        var html = $("#tableControlTest1Header").html();
        $win.log(html + "****");
        new ITabControl({
            navId: "tableControlTest1Header",
            contentId: "tableControlTest1Content",
            defaultTabId: "tableControlTestPage_1",
            loadAll: false,
            selectClass: "selectClass",
            normalClass: "normalClass",
            onSelectChanged: function(lastSelectTabId, currentSelectTabId) {
                $win.log(lastSelectTabId);
                $win.log(currentSelectTabId);
            },
            onSelectLoadBefore: function(selectTabId) {
                $win.log(selectTabId);
                this.loadParams = {
                    "tableControlTestPage_1": {
                        "type": "1"
                    },
                    "tableControlTestPage_2": {
                        "type": "2"
                    },
                    "tableControlTestPage_3": {
                        "type": "3"
                    },
                    "tableControlTestPage_4": {
                        "type": "4"
                    }
                };
            },
            onSelectLoaded: function(selectTabId) {
                $win.log(selectTabId + "loaded");
                //                                         var params = this.loadParams[selectTabId];
                //                                         require(['page_tablecontrol_test_page'],function(PageTableControlTestPage){
                //                                            var testPage = new PageTableControlTestPage(params);
                //                                            //to do;
                //                                            //页面加载如 initDom等处理
                //                                         })
            }
        });
        //                $win.showContent();

    };

    PageTableControlTest.prototype.initEvent = function() {
        $.MessageAlert({
            "content": this.msg
        });
    };

    PageTableControlTest.prototype.beforeShowContent = function() {
        var flag = false;
        for (var i = 0; i != this.dataErrorArray.length; i++) {
            var value = this.dataErrorArray[i];
            if (value && typeof value == "boolean") {
                flag = true;
                break;
            }
        }
        flag ? $win.showErrorPage() : $win.showContent();
    };

    return PageTableControlTest;
});
