/*global define alert $win:true*/
define(["jquery"], function($) {
    function PageFooter(params) {
        this.params = params;
    }

    PageFooter.prototype.initDom = function() {

    };

    PageFooter.prototype.initData = function() {

    };

    PageFooter.prototype.initEvent = function() {
        $("#footershow").unbind("click").bind("click", function() {
            $win.footer.setText("我是Footer");
            $win.footer.show();
            $win.footer.setOnClick(function() {
                alert("点击footer..");
            });
        });
        $("#footerhide").unbind("click").bind("click", function() {
            $win.footer.hide();
        });
    };

    return PageFooter;
});
