/*global define $win:true*/
define(["jquery"], function($) {
    function PageHome(params) {
        this.params = params;
        this.content = "这里是首页的内容";
        this.msg = "初始化homepage成功";

        this.dataErrorArray = new Array();
    }

    PageHome.prototype.initDom = function() {
        $win.getTopbar().setTitle("测试标题");
        $("#homepageContent").children("h2").text(this.content);
    };

    PageHome.prototype.initData = function() {
    };

    PageHome.prototype.initEvent = function() {
        $.MessageAlert({
            "content": this.msg
        });
        $("#demo").unbind("click").bind("click", function() {
            $win.showPage("demo/page/page_demo.html", {
                showTopbar: true,
                moduleName: "page_demo"
            }, true);
        });

        $("#demo_newwin_inner").unbind("click").bind("click", function() {
            $win.showNewWin("demo/page/page_newwin.html", {
                moduleName: "page_newwin",
                pageType: 0,
                showNativeTopbar: false,

                showTopbar: true,
                name: "wildish"
            }, function(data) {
                $win.log("测试成功:" + JSON.stringify(data));
            });
        });

        $("#demo_newwin_outer").unbind("click").bind("click", function() {
            $win.showNewWin("http://www.baidu.com", {
                pageType: 1,
                showNativeTopbar: true,
                nativeTitle: "百度"
            });
        });
    };

    PageHome.prototype.beforeShowContent = function() {
        var flag = false;
        for (var i = 0; i != this.dataErrorArray.length; i++) {
            var value = this.dataErrorArray[i];
            if (value && typeof value == "boolean") {
                flag = true;
                break;
            }
        }
        flag ? $win.showErrorPage() : $win.showContent();
    };

    return PageHome;
});
