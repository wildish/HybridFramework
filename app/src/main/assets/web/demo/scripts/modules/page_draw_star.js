/*global define $win Topbar:true*/
define(["jquery", "stars"], function($, IStars) {
    function PageDrawStar(params) {
        this.params = params;
        this.content = "下面是drawStar页面的内容";
        this.msg = "测试成功";
        this.dataErrorArray = new Array();
    }

    PageDrawStar.prototype.initDom = function() {
        // this._initStarDom("draw-star-container");
        // this._initStarDom("draw-star-container1");
        new IStars({
            wrap: "draw-star-container",
            count: 5,
            color:"red",
            borderColor:"red",
            onSelect: function(i) {
                $.MessageAlert({
                    "content": i + 1 + "星"
                });
            }
        });
        new IStars({
            wrap: "draw-star-container1",
            count: 6,
            onSelect: function(i) {
                $.MessageAlert({
                    "content": i + 1 + "星"
                });
            }
        });
        new IStars({
            wrap: "draw-star-container2",
            count: 5,
            color:"red",
            borderColor:"red",
            onSelect: function(i) {
                $.MessageAlert({
                    "content": i + 1 + "星"
                });
            }
        });
    };

    PageDrawStar.prototype.initData = function() {
        var topBarTxt = "测试drawStar";
        $win.topbar.setTitle(topBarTxt);
        $win.topbar.removeItem(Topbar.KEY_MENU);
        $win.topbar.removeItem(Topbar.KEY_BACKHOME);
        $win.topbar.removeItem(Topbar.KEY_BACKWARD);
        $win.topbar.addBackward();
    };

    PageDrawStar.prototype.initEvent = function() {
        $.MessageAlert({
            "content": this.msg
        });
        // this._initStarEvent("draw-star-container");
        // this._initStarEvent("draw-star-container1");
        // var _this = this;
        // $("#draw-star-container").children("canvas").each(function(index, e) {
        //     $(e).unbind("click").click(function() {
        //         var i = $("#draw-star-container").children("canvas").index(this);
        //         _this._drawStar(this, true);
        //         var expr1 = "canvas:gt(" + i + ")";
        //         $("#draw-star-container").children(expr1).each(function() {
        //             _this._drawStar(this, false);
        //         });
        //         var expr2 = "canvas:lt(" + i + ")";
        //         $("#draw-star-container").children(expr2).each(function() {
        //             _this._drawStar(this, true);
        //         });
        //     });
        // });
    };

    PageDrawStar.prototype._initStarDom = function(wrap) {
        var _this = this;
        var $wrap = $("#" + wrap);
        $wrap.children("canvas").each(function(index, e) {
            _this._drawStar(e, false);
        });
    };

    PageDrawStar.prototype._initStarEvent = function(wrap) {
        var _this = this;
        var $wrap = $("#" + wrap);
        $wrap.children("canvas").each(function(index, e) {
            $(e).unbind("click").click(function() {
                var i = $wrap.children("canvas").index(this);
                _this._drawStar(this, true);
                var expr1 = "canvas:gt(" + i + ")";
                $wrap.children(expr1).each(function() {
                    _this._drawStar(this, false);
                });
                var expr2 = "canvas:lt(" + i + ")";
                $wrap.children(expr2).each(function() {
                    _this._drawStar(this, true);
                });
            });
        });
    };

    PageDrawStar.prototype.beforeShowContent = function() {
        var flag = false;
        for (var i = 0; i != this.dataErrorArray.length; i++) {
            var value = this.dataErrorArray[i];
            if (value && typeof value == "boolean") {
                flag = true;
                break;
            }
        }
        flag ? $win.showErrorModule() : $win.showContent();
    };

    PageDrawStar.prototype._drawStar = function(dom, isSelect) {
        var ctx = dom.getContext('2d');
        if (isSelect) {
            ctx.fillStyle = "#827839";
        } else {
            ctx.fillStyle = "#fff";
        }
        //  ctx.shadowColor="#000000";
        //  ctx.shadowOffsetX=6;
        //  ctx.shadowOffsetY=6;
        //  ctx.shadowBlur=9;
        var w = dom.clientWidth;
        var h = dom.clientHeight;
        w = $(dom).width();
        h = $(dom).height();
        ctx.clearRect(0, 0, w, h);
        var du = Math.PI / 180;
        var d = Math.min(w, h) * (Math.sin(72 * du) / (1 + Math.sin(54 * du)));
        if (d > w / 2.0) {
            d = w / 2.0;
        }
        var diy = (h - (1 + Math.sin(54 * du)) / Math.sin(72 * du) * d) / 2.0;
        var dix = (w - 2 * d) / 2.0;
        ctx.beginPath();
        ctx.moveTo(d + dix, 0 + diy);
        ctx.lineTo((1 - 1 / (Math.tan(72 * du) * Math.tan(54 * du))) * d + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo(0 + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo((1 - Math.sin(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + dix, (1 / Math.sin(72 * du) + Math.cos(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + diy);
        ctx.lineTo(((1 - Math.cos(54 * du)) / Math.sin(72 * du)) * d + dix, ((1 + Math.sin(54 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo(d + dix, (1 / Math.sin(72 * du) + 1 / (Math.tan(72 * du) * Math.sin(54 * du))) * d + diy);
        ctx.lineTo(((1 + Math.cos(54 * du)) / Math.sin(72 * du)) * d + dix, ((1 + Math.sin(54 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo((1 + Math.sin(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + dix, (1 / Math.sin(72 * du) + Math.cos(72 * du) / (Math.tan(72 * du) * Math.sin(54 * du))) * d + diy);
        ctx.lineTo(2 * d + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.lineTo((1 + 1 / (Math.tan(72 * du) * Math.tan(54 * du))) * d + dix, ((1 - Math.cos(72 * du)) / Math.sin(72 * du)) * d + diy);
        ctx.closePath();
        ctx.fill();
        if (!isSelect) {
            ctx.strokeStyle = "#827839";
            ctx.stroke();
        }
    };

    return PageDrawStar;
});
