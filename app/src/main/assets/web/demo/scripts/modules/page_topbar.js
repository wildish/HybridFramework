/*global define $win:true*/
define(["jquery"], function($) {
    function PageTopBar(params) {
        this.params = params;
    }

    PageTopBar.prototype.initDom = function() {

    };

    PageTopBar.prototype.initData = function() {

    };

    PageTopBar.prototype.initEvent = function() {
        $("#topBarshow").unbind("click").bind("click", function() {
            $win.topbar.setTitle("我是标题");
            $win.topbar.show();
        });
        $("#topBarhide").unbind("click").bind("click", function() {
            $win.topbar.hide();
        });
    };

    return PageTopBar;
});
