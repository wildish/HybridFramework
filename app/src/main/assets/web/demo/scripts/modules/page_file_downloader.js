/*global define $win:true*/
define(['jquery'], function($) {
    function PageFolderInfo(params) {
        this.params = params;
        this.resultScroller = null;
    }

    PageFolderInfo.prototype.initDom = function() {
        $("#wrapper").height($win.height - $win.getTopbar().getHeight() - 50 - 100);
        this.resultScroller = $win.touchScroll5("#wrapper");
        $win.getTopbar().setTitle("通用下载器");
    };

    PageFolderInfo.prototype.initData = function() {};

    PageFolderInfo.prototype.initEvent = function() {
        var _this = this;
        $("#loadImage").unbind("click").bind("click", function() {
            $("#content").html("");
            var html = "<div style=\"width:100%;height:100%;\" file-loader='true' image-scale='false' image-type='fitCenter' file-type='pic' file-url='http://icon.nipic.com/BannerPic/20160812/home/20160812130029.jpg'></div>";
            $("#content").append(html);
            $win.getDefaultProxy().initDownload($("[file-loader='true']"));
        });
        $("#loadAudio").unbind("click").bind("click", function() {
            $("#content").html("");
            var html = "<div style=\"width:100%;height:100%;\" file-loader='true' file-type='audio' file-url='http://ps-public.oss-cn-hangzhou.aliyuncs.com/test/ring.mp3'></div>";
            $("#content").append(html);
            $win.getDefaultProxy().initDownload($("[file-loader='true']"), {
                onComplete: function(task) {
                    $win.log("下载成功，文件保存在" + task.path);
                    _this.showResult("下载成功，文件保存在" + task.path);
                }
            });
        });
        $("#loadFile").unbind("click").bind("click", function() {
            $win.getDefaultProxy().initDownload($("[file-loader='true']"), {
                onPaused: function() {

                },
                onComplete: function() {

                },
                onProgress: function() {

                },
                onError: function() {

                }
            });
        });
    };

    PageFolderInfo.prototype.showResult = function(msg) {
        $(".scroller").append("<div class='col-xs-12'><h4>"+msg+"</h4></div>");
        this.resultScroller.refresh();
    };


    return PageFolderInfo;
});
