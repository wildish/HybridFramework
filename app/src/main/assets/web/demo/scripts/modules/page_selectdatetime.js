/*global define define:true window $win Topbar*/
define(["jquery", "selectdatetime"], function($, SelectDateTime) {
    function PageSelectDatetime(params) {
        this.params = params;
        this.selectValue = null;
    }

    PageSelectDatetime.prototype.initDom = function() {
        var _this = this;
        $win.topbar.setTitle("时间选择");
        $win.topbar.addTextMenuItem("menu_ok", Topbar.MENU_RIGHT, "确定", function() {
            if ($.isNotBlank(_this.selectValue)) {
                console.log("您选择的时间是..");
            } else {
                console.log("请选择时间...");
            }
        });
        $("#date_wrapper").height($(window).height() - 50);

    };

    PageSelectDatetime.prototype.initData = function() {};

    PageSelectDatetime.prototype.initEvent = function() {
        new SelectDateTime({
            wrap: "date_wrapper",
            isMonthDefault: true,
            monthArray: ['1991-01', '2017-05'],
            isMultiSelect: true,
            isHasTime: false,
            initDateTime: ['2016-07-09', '2016-9-14']
        });
    };

    return PageSelectDatetime;
});
