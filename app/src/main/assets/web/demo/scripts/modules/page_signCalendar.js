/*global define $win:true*/
define(["jquery"], function($) {
    function PageSignCalendar(params) {
        this.params = params;
        this.signValue = new Array();
    }

    PageSignCalendar.prototype.initDom = function() {

    };

    PageSignCalendar.prototype.initData = function() {
        var value = "1,2,3,5,21,24,30"
        var signStrArray = value.split(",");
        for (var i = 0; i < signStrArray.length; i++) {
            var obj = {};
            obj.signDay = signStrArray[i];
            this.signValue.push(obj);
        }
    };

    PageSignCalendar.prototype.initEvent = function() {
        var _this = this;
        $("#signCalendar").unbind("click").bind("click", function() {
            $.SignCalendar(_this.signValue);
        });
    };

    return PageSignCalendar;
});
