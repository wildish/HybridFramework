/*global define $win:true*/
define(['jquery'], function($) {
    function PageMock(params) {
        this.params = params;
    }

    PageMock.prototype.initDom = function() {
        $win.getTopbar().setTitle("数据请求演示");
    };

    PageMock.prototype.initData = function() {};

    PageMock.prototype.initEvent = function() {
        $("#login").unbind("click").bind("click", function() {
            $win.getDefaultProxy().getData({
                "url": {
                    "local": "/mock/login",
                    "remote": "/mock/login"
                },
                "account": "keith",
                "pass": 111,
                "verify": "112"
            }, function(result) {
                if (result.success) {
                    $("#result").append("<div class='col-xs-12'><h4>" + result.msg + "</h4></div>");
                } else {
                    $.MessageAlert({
                        "content": result.result
                    });
                }
            });
        });

        $("#userInfo").unbind("click").bind("click", function() {
            $win.getDefaultProxy().getData({
                "url": {
                    "local": "/mock/user/info/123",
                    "remote": "/mock/user/info/123"
                }
            }, function(result) {
                if (result.success) {
                    $("#result").append("<div class='col-xs-12'><h4>" + JSON.stringify(result) + "</h4></div>");
                } else {
                    $.MessageAlert({
                        "content": result.result
                    });
                }
            });
        });

        $("#userList").unbind("click").bind("click", function() {
            $win.getDefaultProxy().getData({
                "url": {
                    "local": "/mock/user/list",
                    "remote": "/mock/user/list"
                },
                pagesize: 5,
                offset: 0
            }, function(result) {
                if (result.success) {
                    $("#result").append("<div class='col-xs-12'><h4>" + JSON.stringify(result) + "</h4></div>");
                } else {
                    $.MessageAlert({
                        "content": result.result
                    });
                }
            });
        });
    };

    return PageMock;
});
