/*global define console $win window Topbar:true*/
define(["jquery", "swiper-gallery"], function($) {
    function PageGallery(params) {
        this.params = params;
        this.myScroll = null;
        this.galleryData = null;
    }

    PageGallery.prototype.initDom = function() {
        $win.getTopbar().setTitle("gallery控件");
        $("#wrapper").height($(window).height() - 50);
    };

    PageGallery.prototype.initData = function() {
        var imageList = new Array();
        imageList.push({url: "http://icon.nipic.com/BannerPic/20160824/home/20160824153208.jpg"});
        imageList.push({url: "http://icon.nipic.com/BannerPic/20160811/home/20160811085257.jpg"});
        imageList.push({url: "http://icon.nipic.com/BannerPic/20160812/home/20160812130029.jpg"});
        this.galleryData = imageList;
        var html = "";
        for (var i = 0; i < imageList.length; i++) {
            html += "<div class='swiper-slide'>";
            html += "    <div style=\"width:100%;height:100%;\" file-loader='true' image-scale='false' image-type='fitCenter' file-type='pic' file-url='" + imageList[i].url + "'></div>";
            html += "</div>";
        }
        $("#swiper-wrapper").html("").html(html);
    };

    PageGallery.prototype.initEvent = function() {
        var _this = this;
        $win.getDefaultProxy().initDownload($("[file-loader='true']"));
        _this.myGallery = $("#galleryWrapper").swiper({
            autoplay: 3000,
            pagination: ".swiper-pagination",
            onTap: function(swiper, event) {
                event.preventDefault();
                var showImages = {
                    images: _this.galleryData,
                    index: swiper.activeIndex
                };
                _this.createFunc(showImages);
            }
        });
    };

    //点击图片显示缩略图
    PageGallery.prototype.createFunc = function(params) {
        $win.getDefaultProxy().showImages(params);
    };

    return PageGallery;
});
