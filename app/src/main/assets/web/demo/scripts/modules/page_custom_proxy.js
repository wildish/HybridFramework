/*global define $win:true*/
define(['jquery'], function($) {
    function PageFolderInfo(params) {
        this.params = params;
    }

    PageFolderInfo.prototype.initDom = function() {
    };

    PageFolderInfo.prototype.initData = function() {};

    PageFolderInfo.prototype.initEvent = function() {
        $("#pay").unbind("click").bind("click", function() {
            $win.getProxy("proxy_alipay").doTest(function(result) {
                if(result.success) {
                    $("#result").append("<div class='col-xs-12'><h5>支付成功</h5></div>");
                } else {
                    $("#result").append("<div class='col-xs-12'><h5>好吧，虽然支付失败了，但是调用是成功了，是吧^_^</h5></div>");
                }
            });
        });
    };

    return PageFolderInfo;
});
