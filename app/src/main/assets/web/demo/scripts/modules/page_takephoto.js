/*global define $win:true*/
define(["jquery"], function($) {
    function PageTakePhoto(params) {
        this.params = params;
    }

    PageTakePhoto.prototype.initDom = function() {
        $("#pageContainer").width($win.width);
        $("#pageContainer").height($win.height - $win.getTopbar().getHeight());

        // 封装
        $win.getDefaultProxy().takePhoto(function(result) {
            if (result.success && result.hasOwnProperty("filepath")) {
                if(result.filepath) {
                    $("#pageContainer").append("<div style=\"width:100%;height:90%;\" file-loader='true' image-scale='false' image-type='fitCenter' file-type='pic' file-url='" + result.filepath + "'></div>");
                }
                $win.initDownload($("[file-loader='true']"));
            } else {
                $win.log("请求失败，" + result.msg);
            }
        });
    };

    PageTakePhoto.prototype.initData = function() {

    };

    PageTakePhoto.prototype.initEvent = function() {
    };

    return PageTakePhoto;
});
