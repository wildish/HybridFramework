/*global define $win:true*/
define(["jquery"], function($) {
    function PageScanQrcode(params) {
        this.params = params;
    }

    PageScanQrcode.prototype.initDom = function() {
        $win.getDefaultProxy().scanQrcode(function(data) {
            if (data.success) {
                var pwd = data.code;
                $("#resultContainer").html(pwd);
            } else {
                $.MessageAlert({
                    type: "error",
                    content: "扫描二维码失败!",
                    timer: 2000
                });
            }
        });
    };

    PageScanQrcode.prototype.initData = function() {

    };

    PageScanQrcode.prototype.initEvent = function() {

    };

    return PageScanQrcode;
});
