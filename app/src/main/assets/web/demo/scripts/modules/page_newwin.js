/*global define $win:true*/
define(['jquery'], function($) {
    function PageNewwin(params) {
        this.params = params;
    }

    PageNewwin.prototype.initDom = function() {};

    PageNewwin.prototype.initData = function() {};

    PageNewwin.prototype.initEvent = function() {
        var _this = this;
        $("#goback").unbind("click").bind("click", function() {
            var callbackParams = {
                result: "congratulations, " + _this.params.name
            };
            $win.cancel(callbackParams);
        });
        $("#gonext").unbind("click").bind("click", function() {
            $win.showNewWin("demo/page/page_newwin_2.html", {
                moduleName: "page_newwin_child",
                pageType: 0,
                showNativeTopbar: false,

                showTopbar: true,
                name: "child " + _this.params.name
            }, function(data) {
                $win.log("获取子页面结果:" + JSON.stringify(data));
            });
        });
    };

    return PageNewwin;
});
