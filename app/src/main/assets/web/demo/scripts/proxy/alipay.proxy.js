define([], function() {
    function AlipayProxy(scheme, helper) {
        this.helper = helper;
        this.scheme = scheme;
        this.proxyName = "proxy_alipay";
    }
    AlipayProxy.ACTION_PAY = "action_pay";

    /**
     * 调用支付宝移动支付功能
     * params:
     *      traderNo: 订单号，多个订单号使用"_"隔开,最长64位
     *      subject: 商品名称，最长128位
     *      totalFee: 总额
     *      body: 商品详情，最长512位
     *
     *      额外参数，预留，暂不支持
     *      app_id: string类型，平台相关ios/android
     *      appenv: string类型，标识客户端来源，可以不填
     */
    AlipayProxy.prototype.doTest = function(callback) {
        var params = {
            "traderNo": "45895141354133654",
            "subject": "测试商品",
            "totalFee": 0.01,
            "body": "这是商品支付功能的简单测试"
        };

        if (!params || !params.traderNo || !params.subject || !params.totalFee || !params.body) {
            $win.log("请求参数不全");
            return;
        }

        this.helper.proxyExecute(this.scheme, this.proxyName, AlipayProxy.ACTION_PAY, params, callback);
    };

    return AlipayProxy;
});