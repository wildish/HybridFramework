/**
 * Config对象，系统通用配置函数
 * 可配置的参数包括：
 * 	server: 请求数据的服务器地址
 *  mainpage: 首页
 */
/*global define window Base64 :true*/
define(["jquery"], function($) {
    function Config() {
        // true表示访问node_server, false表示由系统框架转发
        this.debug = false;
        this.node_server = window.location.origin + "/web/";
        this.server = "file:///android_asset/web/";

        this.debugRequestPath = window.location.origin;

        this.project = "demo";
        this.basepage = this.project + "/main.html";
        this.homepage = {
            "url": this.project + "/homepage.html",
            "moduleName": "page_home"
        };
        this.proxyList = [{
            "name": "proxy_alipay",
            "path": "proxy/alipay.proxy"
        }];
    }

    Config.prototype.getDebugReqUrl = function(url) {
        return this.debugRequestPath + url;
    };

    Config.prototype.getHomepage = function() {
        return this.getUrl(this.homepage);
    };

    Config.prototype.getUrl = function(path, params) {
        var paramStr = "";
        if ($.isNotBlank(params)) {
            paramStr = Base64.encode(JSON.stringify(params));
        }
        if(this.debug) {
            return this.node_server + this.basepage + "?_p=" + Base64.encode(path) + "&_a=" + paramStr;
        } else {
            return this.server + this.basepage + "?_p=" + Base64.encode(path) + "&_a=" + paramStr;
        }
    };

    Config.prototype.getLocation = function(page) {
        if(this.debug) {
            return this.node_server + page;
        } else {
            return this.server + page;
        }
    };

    return Config;
});
