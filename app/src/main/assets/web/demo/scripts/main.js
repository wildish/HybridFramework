/*global requirejs $ window Base64 document $win:true*/
(function() {
    require.config({
        paths: {
            // libs
            "jquery": "../../common/scripts/lib/jquery/jquery-2.2.1.min",
            "hybrid_helper": "../../common/scripts/lib/hybrid/hybrid_helper",
            "masonry": "../../common/scripts/lib/masonry.pkgd",
            "history": "../../common/scripts/lib/history/jquery.history.min",
            "iscroll": "../../common/scripts/lib/iscroll/iscroll",
            "iscroll-probe": "../../common/scripts/lib/iscroll/iscroll-probe",
            "touch": "../../common/scripts/lib/baiduapi/touch-0.2.14.min",
            "bootstrap": "../../common/scripts/lib/bootstrap/js/bootstrap.min",
            "bootstrap-switch": "../../common/scripts/lib/bootstrap-switch/js/bootstrap-switch.min",
            "bootstrap-datetimepicker": "../../common/scripts/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min",
            "bootstrap-datetimepicker-zhcn": "../../common/scripts/lib/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN",
            "bootstrap-select": "../../common/scripts/lib/bootstrap-select/js/bootstrap-select.min",
            "jquery-knob": "../../common/scripts/lib/jquery.knob/jquery.knob.min",
            "qrcode": "../../common/scripts/lib/qrcode/jquery.qrcode",
            "swiper-gallery": "../../common/scripts/lib/swiper/js/swiper-3.4.0.jquery.min",

            // utils
            "extend": "../../common/scripts/utils/extend_js",
            "ajax": "../../common/scripts/utils/ajax",
            "waiting": "../../common/scripts/utils/waiting",
            "messagealert": "../../common/scripts/utils/messagealert",
            "messagebox": "../../common/scripts/utils/messagebox",
            "iscrolllist": "../../common/scripts/utils/list.iscroll",
            "iscroll5list": "../../common/scripts/utils/list.iscroll5",
            "itabcontrol": "../../common/scripts/utils/tabcontrol",
            "itablecontrol": "../../common/scripts/utils/tablecontrol",
            "igallery": "../../common/scripts/utils/gallery",
            "mainwin": "../../common/scripts/utils/mainwin",
            "topbar": "../../common/scripts/utils/topbar",
            "shoppingcartItem": "../../common/scripts/utils/shoppingcart_item",
            "footer": "../../common/scripts/utils/footer",
            "signcalendar": "../../common/scripts/utils/sign_calendar",
            "selectdatetime": "../../common/scripts/utils/select.datetime",
            "stars": "../../common/scripts/utils/stars",

            // modules
            "config": "config",
            "page_picklocation": "modules/page_picklocation",
            "page_newwin": "modules/page_newwin",
            "page_newwin_child": "modules/page_newwin_child",
            "page_home": "modules/page_home",
            "page_mock": "modules/page_mock",
            "page_demo": "modules/page_demo",
            "page_signCalendar": "modules/page_signCalendar",
            "page_waiting": "modules/page_waiting",
            "page_topbar": "modules/page_topbar",
            "page_footer": "modules/page_footer",
            "page_msgbox": "modules/page_msgbox",
            "page_msgalert": "modules/page_msgalert",
            "page_takephoto": "modules/page_takephoto",
            "page_takealbum": "modules/page_takealbum",
            "page_scanqrcode": "modules/page_scanqrcode",
            "page_listiscroll": "modules/page_listiscroll",
            "page_gallery": "modules/page_gallery",
            "page_selectdatetime": "modules/page_selectdatetime",
            "page_tablecontrol_test": "modules/page_tablecontrol_test",
            "page_tablecontrol_test_a": "modules/page_tablecontrol_test_a",
            "page_tablecontrol_test_b": "modules/page_tablecontrol_test_b",
            "page_tablecontrol_test_c": "modules/page_tablecontrol_test_c",
            "page_tablecontrol_test1": "modules/page_tablecontrol_test1",
            "page_tablecontrol_test_1": "modules/page_tablecontrol_test_1",
            "page_tablecontrol_test_2": "modules/page_tablecontrol_test_2",
            "page_tablecontrol_test_3": "modules/page_tablecontrol_test_3",
            "page_tablecontrol_test_4": "modules/page_tablecontrol_test_4",
            "page_draw_star": "modules/page_draw_star",
            "page_touchscroll": "modules/page_touchscroll",
            "page_showimages": "modules/page_showimages",
            "page_folder_info": "modules/page_folder_info",
            "page_file_downloader": "modules/page_file_downloader",
            "page_custom_proxy": "modules/page_custom_proxy",
        },
        shim: {
            "iscroll": {
                "deps": []
            },
            "iscroll-probe": {
                "deps": []
            },
            "swiper-gallery": {
                "deps": ["jquery"]
            },
            "touch": {
                "deps": []
            },
            "bootstrap": {
                "deps": ["jquery"]
            },
            "bootstrap-switch": {
                "deps": ["bootstrap"]
            },
            "bootstrap-datetimepicker": {
                "deps": ["bootstrap"]
            },
            "bootstrap-datetimepicker-zhcn": {
                "deps": ["bootstrap-datetimepicker"]
            },
            "bootstrap-select": {
                "deps": ["bootstrap"]
            },
            "jquery-knob": {
                "deps": ["jquery"]
            },
            "messagebox": {
                deps: ["jquery"]
            },
            "messagealert": {
                deps: ["jquery"]
            },
            "waiting": {
                deps: ["jquery"]
            },
            "extend": {
                deps: ["jquery", "masonry"]
            }
        }
    });

    requirejs.onError = function(err) {
        if (typeof $win != "undefined" && $win) {
            $win.log("requirejs:" + err.stack);
            $win.showErrorPage();
        } else {
            console.log(err.stack);
        }
    };

    require(["iscrolllist", "iscroll5list", "itabcontrol", "signcalendar", "igallery", "mainwin", "extend", "waiting", "messagealert", "messagebox", "bootstrap"],
        function(IScrollList, IScroll5List, ITabcontrol, SignCalendar, IGallery, MainWin) {

            document.addEventListener("touchmove", function(e) {
                // 避免屏蔽bootstrap-select
                var target = e.target;
                if (target.tagName == "A" && target.attributes["data-tokens"]) {
                    return;
                }
                e.preventDefault();
            }, false);

            window.IScrollList = IScrollList;
            window.IScroll5List = IScroll5List;
            window.ITabcontrol = ITabcontrol;
            window.SignCalendar = SignCalendar;
            window.IGallery = IGallery;
            var width = $(window).width();
            var height = $(window).height();
            window.$win = new MainWin(width, height);
            $win.initProxy();

            // 初始化页面参数
            var page = "";
            var pattern = new RegExp("_p=([^&]*)");
            var result = pattern.exec(window.location.href);
            if (result != null) {
                if ($.isNotBlank(result[1])) {
                    page = Base64.decode(result[1]);
                }
            }

            if ($.isNotBlank(page)) {
                var params = "{}";
                result = new RegExp("_a=([^&]*)").exec(window.location.href);
                if (result != null) {
                    params = Base64.decode(result[1]);
                }
                $win.showPage(page, JSON.parse(params), false);
            } else {
                $win.log("未设置page参数，加载config中设置的首页");
                $win.loadHomepage();
            }
        });
})(window);
