#-------------------------------------------定制化区域----------------------------------------------
#---------------------------------1.实体类---------------------------------
-keep class com.longtu.gongche.ebo.**  {* ;}
-keep class com.longtu.gongche.model.**  {* ;}
-keep class com.longtu.Entry.**  {* ;}
#-------------------------------------------------------------------------

#---------------------------------2.第三方包-------------------------------
# imageloader
-keep class com.nostra13.universalimageloader.** { *; }
# datetimepicker
-keep class com.longtu.datetimepicker.** { *; }
# imagepicker
-keep class com.chinark.apppickimagev3.** { *; }
# photoview
-keep class com.bm.library.** { *; }
# zxing
-keep class com.zxing.** { *; }
# 高德地图
-keep class com.amap.api.location.**{*;}
-keep class com.amap.api.fence.**{*;}
-keep class com.autonavi.aps.amapapi.model.**{*;}
-dontwarn com.amp.apis.lib.**
-keep class com.amp.apis.lib.** {*;}
-dontwarn com.autonavi.amap.**
-keep class com.autonavi.amap.** {*;}
# 信鸽
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep class com.tencent.android.tpush.**  {* ;}
-keep class com.tencent.mid.**  {* ;}
#alipay
#-libraryjars libs/alipaySDK-20160516.jar
-keep class com.alipay.android.app.IAlixPay{*;}
-keep class com.alipay.android.app.IAlixPay$Stub{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback{*;}
-keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
-keep class com.alipay.sdk.app.PayTask{ public *;}
-keep class com.alipay.sdk.app.AuthTask{ public *;}
#第三方包
#dom4j
-dontwarn org.dom4j.**
-keep class org.dom4j.** { *;}
#commons-lang-2.5
-dontwarn org.apache.commons.lang.**
-keep class org.apache.commons.lang.** { *;}
#httpclient
-dontwarn org.apache.http.**
-keep class org.apache.http.** { *;}
#jaxen
-dontwarn org.jaxen.**
-keep class org.jaxen.** { *;}
#javel
-dontwarn net.sourceforge.jeval.**
-keep class net.sourceforge.jeval.** { *;}
#jg_filter_sdk
-dontwarn com.jg.**
-keep class com.jg.** { *;}
#mrf-map
-dontwarn com.mrf.**
-keep class com.mrf.** { *;}
#pinyin4j
-dontwarn com.hp.hpl.sparta.**
-dontwarn net.sourceforge.pinyin4j.**
-dontwarn pinyindb.**
-dontwarn demo.**
-keep class com.hp.hpl.sparta.** { *;}
-keep class net.sourceforge.pinyin4j.** { *;}
-keep class pinyindb.** { *;}
-keep class demo.** { *;}
#imageloader
-dontwarn com.nostra13.universalimageloader.**
-keep class com.nostra13.universalimageloader.** { *;}
#wup
-dontwarn com.qq.taf.jce.**
-keep class com.qq.taf.jce.** { *;}
#file downloader
-dontwarn com.liulishuo.filedownloader.**
-keep class com.liulishuo.filedownloader.** { *;}
#error for google
-dontwarn com.google.common.**
-keep class com.google.common.** { *;}
-dontwarn okhttp3.**
-keep class okhttp3.** { *;}
-dontwarn okio.**
-keep class okio.** { *;}

#-------------------------------------------------------------------------

#---------------------------------3.webview与js互相调用的类------------------------
-keepclassmembers class com.longtu.jsbinding.demo.** {*;}
-keepclassmembers class * extends android.webkit.webViewClient {
    public *;
}
-keepclassmembers class * extends android.webkit.WebChromeClient {
    public *;
}
#-------------------------------------------------------------------------

#---------------------------------4.反射相关的类和方法-----------------------
-keep class com.longtu.gongche.requesthandler.**{*;}
#----------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

#---------------------------------基本指令区----------------------------------
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-printmapping proguardMapping.txt
-optimizations !code/simplification/cast,!field/*,!class/merging/*
-keepattributes *Annotation*,InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
#----------------------------------------------------------------------------

#---------------------------------默认保留区---------------------------------
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Appliction
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends android.view.View
-keep public class com.android.vending.licensing.ILicensingService
-keep class android.support.** {*;}

-keepclasseswithmembernames class * {
    native <methods>;
}
-keepclassmembers class * extends android.app.Activity{
    public void *(android.view.View);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep public class * extends android.view.View{
    *** get*();
    void set*(***);
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
-keep class **.R$* {
 *;
}
-keepclassmembers class * {
    void *(**On*Event);
}
#----------------------------------------------------------------------------
#-------------------------------------其他-----------------------------------
# 保持测试相关的代码
-dontnote junit.framework.**
-dontnote junit.runner.**
-dontwarn android.test.**
-dontwarn android.support.test.**
-dontwarn org.junit.**
#如果有引用v4包可以添加下面这行
-keep class android.support.v4.** { *; }
-keep public class * extends android.support.v4.**
-keep public class * extends android.app.Fragment
-keep public class * extends android.support.v4.app.Fragment
#如果引用了v4或者v7包，可以忽略警告，因为用不到android.support
-dontwarn android.support.**
#xUtils(保持注解，及使用注解的Activity不被混淆，不然会影响Activity中你使用注解相关的代码无法使用)
-keep class * extends java.lang.annotation.Annotation {*;}
-keep class com.otb.designerassist.activity.** {*;}
#----------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------