/*
 * Created by Wildish on 2016-10-28
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 *
 */

package com.wildish.customprogressdialog;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}