/*
 * Created by Wildish on 2016-10-28
 *
 * Contact me with keithknight@qq.com
 *
 * Copyright (c) 2016 Wildish
 *
 */

package com.wildish.customprogressdialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;

public class CustomProgressDialog extends Dialog {
    private static CustomProgressDialog customProgressDialog = null;

    @Deprecated
    public CustomProgressDialog(Context context) {
        super(context);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public static CustomProgressDialog createDialog(Context context) {
        try {
            customProgressDialog = new CustomProgressDialog(context,
                    R.style.CustomProgressDialog);
            customProgressDialog.setContentView(R.layout.customprogressdialog);
            return customProgressDialog;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
